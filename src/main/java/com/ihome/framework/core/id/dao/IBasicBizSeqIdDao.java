package com.ihome.framework.core.id.dao;

import org.apache.ibatis.annotations.Param;

import com.ihome.framework.core.id.domain.SysSequence;

public interface IBasicBizSeqIdDao {

	public long nextval(@Param("seqName") String seqName);

	public long currval(@Param("seqName") String seqName);

	public int insert(SysSequence sequence);
}
