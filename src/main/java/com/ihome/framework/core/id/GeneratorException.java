package com.ihome.framework.core.id;

import com.ihome.framework.core.exception.ApplicationException;
import com.ihome.framework.core.exception.PlatformException;

public class GeneratorException extends PlatformException {

	private static final long serialVersionUID = -1952313675987709080L;

	public GeneratorException(String message, Throwable cause) {
		super(message, cause);
	}

}
