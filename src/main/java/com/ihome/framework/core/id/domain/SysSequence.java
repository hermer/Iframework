package com.ihome.framework.core.id.domain;

public class SysSequence {
	private String seqName;
	private long currentVal;
	private long stepVal;

	public SysSequence(String seqName, long currentVal, long stepVal) {
		this.seqName = seqName;
		this.currentVal = currentVal;
		this.stepVal = stepVal;
	}

	public String getSeqName() {
		return seqName;
	}

	public void setSeqName(String seqName) {
		this.seqName = seqName;
	}

	public long getCurrentVal() {
		return currentVal;
	}

	public void setCurrentVal(long currentVal) {
		this.currentVal = currentVal;
	}

	public long getStepVal() {
		return stepVal;
	}

	public void setStepVal(long stepVal) {
		this.stepVal = stepVal;
	}

}
