package com.ihome.framework.core.nos;

/**
 * Nos相关配置的封装
 * 
 * @author zhengxiaohong
 *
 */
public class NosConfig {

	private String isOpen;

	private String hostName;

	private String oldHostName;

	private String accessKey;

	private String secretKey;

	private String publicBucket;

	private String privateBucket;

	private String oldAccessKey;

	private String oldSecretKey;

	private String namespace;

	public String getIsOpen() {
		return isOpen;
	}

	public void setIsOpen(String isOpen) {
		this.isOpen = isOpen;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getOldHostName() {
		return oldHostName;
	}

	public void setOldHostName(String oldHostName) {
		this.oldHostName = oldHostName;
	}

	public String getAccessKey() {
		return accessKey;
	}

	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public String getPublicBucket() {
		return publicBucket;
	}

	public void setPublicBucket(String publicBucket) {
		this.publicBucket = publicBucket;
	}

	public String getPrivateBucket() {
		return privateBucket;
	}

	public void setPrivateBucket(String privateBucket) {
		this.privateBucket = privateBucket;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public String getOldAccessKey() {
		return oldAccessKey;
	}

	public void setOldAccessKey(String oldAccessKey) {
		this.oldAccessKey = oldAccessKey;
	}

	public String getOldSecretKey() {
		return oldSecretKey;
	}

	public void setOldSecretKey(String oldSecretKey) {
		this.oldSecretKey = oldSecretKey;
	}

}