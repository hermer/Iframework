package com.ihome.framework.core.nos;

import com.netease.cloud.auth.Credentials;
import com.netease.cloud.services.nos.NosClient;

public class IHomeNosOldClient extends NosClient {

	/**
	 * 新建IHomeNosClient实例
	 */
	public IHomeNosOldClient() {
		super();
	}

	/**
	 * 新建IHomeNosClient实例
	 * 
	 * @param Credentials
	 *            秘钥相关
	 * @param hostName
	 *            nos的服务器名
	 */
	public IHomeNosOldClient(Credentials Credentials, String hostName) {
		super(Credentials);
		setEndpoint(hostName);
	}

}
