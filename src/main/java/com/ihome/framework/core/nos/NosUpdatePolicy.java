package com.ihome.framework.core.nos;

import java.util.HashMap;
import java.util.Map;

import com.ihome.framework.core.utils.JsonUtil;

public class NosUpdatePolicy {

	/**
	 * 桶名
	 */
	private String bucket;

	/**
	 * 对象名
	 */
	private String object;

	/**
	 * token的过期时间
	 */
	private int expire;

	/**
	 * 回调url
	 */
	private String callBackUrl;

	/**
	 * 回调的消息体，支持魔法变量
	 */
	private String callBackBody;

	/**
	 * 文件上传成功后，浏览器执行303跳转的URL
	 */
	private String returnUrl;

	/**
	 * 上传成功后，自定义最终返回给上传端（在指定ReturnUrl时是携带在跳转路径参数中）的数据，支持魔法变量
	 */
	private String returnBody;

	public NosUpdatePolicy() {

	}

	public NosUpdatePolicy(String bucket, String object, int expire) {
		super();
		this.bucket = bucket;
		this.object = object;
		this.expire = expire;
	}

	public NosUpdatePolicy withCallBackUrl(String callbackUrl) {
		this.callBackUrl = callbackUrl;
		return this;
	}

	public NosUpdatePolicy withCallBackBody(String callBackBody) {
		this.callBackBody = callBackBody;
		return this;
	}

	public NosUpdatePolicy withReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
		return this;
	}

	public NosUpdatePolicy withReturnBody(String returnBody) {
		this.returnBody = returnBody;
		return this;
	}

	public String toJson() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("Bucket", bucket);
		map.put("Object", object);
		map.put("Expires", expire);
		map.put("CallBackUrl", callBackUrl);
		map.put("CallBackBody", callBackBody);
		map.put("ReturnUrl", returnUrl);
		map.put("ReturnBody", returnBody);
		return JsonUtil.toJSONString(map);
	}
}
