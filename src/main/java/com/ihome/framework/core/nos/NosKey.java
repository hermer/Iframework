package com.ihome.framework.core.nos;

import org.apache.commons.lang3.StringUtils;

/**
 * nos命名规范的封装
 * 
 * @author zhengxiaohong
 *
 */
public class NosKey {

	/**
	 * namespace 命名空间， 一般按产品规定
	 */
	private String namespace;

	/**
	 * app 应用名
	 */
	private String app;

	/**
	 * source 文件的来源
	 */
	private String source;

	/**
	 * type 文件类型
	 */
	private String type;

	/**
	 * origname, 原始文件名，如果存在的话。如果是新生的文件，填new
	 */
	private String origname;

	/**
	 * 按当前的存储时间生成的时间戳
	 */
	private String timestamp;

	/**
	 * 操作系统的文件类型
	 */
	private String filetype;

	/**
	 * 竖线
	 */
	public static final String BAR = "|";

	/**
	 * 设置namespace
	 * 
	 * @param namespace
	 * @return
	 */
	public NosKey withNamespace(String namespace) {
		this.namespace = namespace;
		return this;
	}

	/**
	 * 设置app
	 * 
	 * @param app
	 * @return
	 */
	public NosKey withApp(String app) {
		this.app = app;
		return this;
	}

	/**
	 * 设置source
	 * 
	 * @param source
	 * @return
	 */
	public NosKey withSource(String source) {
		this.source = source;
		return this;
	}

	/**
	 * 设置type
	 * 
	 * @param type
	 * @return
	 */
	public NosKey withType(String type) {
		this.type = type;
		return this;
	}

	/**
	 * 设置origname
	 * 
	 * @param origname
	 * @return
	 */
	public NosKey withOrigname(String origname) {
		this.origname = origname;
		return this;
	}

	/**
	 * 设置timestamp
	 * 
	 * @param timestamp
	 * @return
	 */
	public NosKey withTimestamp(long timestamp) {
		this.timestamp = String.valueOf(timestamp);
		return this;
	}

	/**
	 * 设置filetype
	 * 
	 * @param filetype
	 * @return
	 */
	public NosKey withFiletype(String filetype) {
		this.filetype = filetype;
		return this;
	}

	/**
	 * 生成key ｛namespace}|{app}|{source}|{type}|{origname}|{timestamp}.{filetype}
	 * 
	 * @return
	 */
	public String toKey() {
		StringBuilder sb = new StringBuilder(namespace);
		sb.append(BAR);

		if (StringUtils.isNotBlank(app)) {
			sb.append(app).append(BAR);
		}
		if (StringUtils.isNotBlank(source)) {
			sb.append(source).append(BAR);
		}
		if (StringUtils.isNotBlank(type)) {
			sb.append(type).append(BAR);
		}
		if (StringUtils.isNotBlank(origname)) {
			sb.append(origname).append(BAR);
		}
		if (StringUtils.isNotBlank(timestamp)) {
			sb.append(timestamp).append(BAR);
		}
		if (StringUtils.isNotBlank(filetype)) {
			sb.append(".").append(filetype).append(BAR);
		}
		return sb.toString();
	}

	/**
	 * 生成key
	 */
	public String toString() {
		return toKey();
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public String getApp() {
		return app;
	}

	public void setApp(String app) {
		this.app = app;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOrigname() {
		return origname;
	}

	public void setOrigname(String origname) {
		this.origname = origname;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getFiletype() {
		return filetype;
	}

	public void setFiletype(String filetype) {
		this.filetype = filetype;
	}

}
