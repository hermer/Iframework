package com.ihome.framework.core.nos;

import java.util.HashMap;
import java.util.Map;

import com.ihome.framework.core.utils.JsonUtil;

public class NosMagicVar {

	/**
	 * 桶名
	 */
	private String bucket;

	/**
	 * 对象名
	 */
	private String object;

	/**
	 * 图片信息
	 */
	private String imageInfo;

	/**
	 * 对象大小，单位字节
	 */
	private String objectSize;

	/**
	 * 对象（文件）的mime类型
	 */
	private String contentType;

	/**
	 * 对象的唯一描述符，目前可能是md5
	 */
	private String etag;

	/**
	 * 上传的原始文件名
	 */
	private String fileName;

	/**
	 * 音视频资源的元信息
	 */
	private String aVinfo;

	public NosMagicVar withBucket() {
		this.bucket = "$(Bucket)";
		return this;
	}

	public NosMagicVar withObject() {
		this.object = "$(Object)";
		return this;
	}

	public NosMagicVar withImageInfo() {
		this.imageInfo = "$(ImageInfo)";
		return this;
	}

	public NosMagicVar withObjectSize() {
		this.objectSize = "$(ObjectSize)";
		return this;
	}

	public NosMagicVar withContentType() {
		this.contentType = "$(ContentType)";
		return this;
	}

	public NosMagicVar withEtag() {
		this.etag = "$(Etag)";
		return this;
	}

	public NosMagicVar withFileName() {
		this.fileName = "$(FileName)";
		return this;
	}

	public NosMagicVar withAVinfo() {
		this.aVinfo = "$(AVinfo)";
		return this;
	}

	public String toJson() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("Bucket", bucket);
		map.put("Object", object);
		map.put("ImageInfo", imageInfo);
		map.put("ObjectSize", objectSize);
		map.put("ContentType", contentType);
		map.put("Etag", etag);
		map.put("FileName", fileName);
		map.put("AVinfo", aVinfo);

		String json = JsonUtil.toJSONString(map);
		json = json.replace("\"$(ImageInfo)\"", "$(ImageInfo)").replace("\"$(ObjectSize)\"", "$(ObjectSize)")
				.replace("\"$(AVinfo)\"", "$(AVinfo)");
		return json;
	}
}
