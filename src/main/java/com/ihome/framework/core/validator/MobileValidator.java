/**
 * 
 */
package com.ihome.framework.core.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.ihome.framework.core.utils.RegExValidateUtils;
import com.ihome.framework.core.validator.constraints.Mobile;

/**
 * 
 * @author zhengxiaohong
 */
public class MobileValidator implements ConstraintValidator<Mobile, String> {

	private final static String mobile = "^1\\d{10}$";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.validation.ConstraintValidator#initialize(java.lang.annotation.
	 * Annotation)
	 */
	@Override
	public void initialize(Mobile constraintAnnotation) {
		// TODO Auto-generated method stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.validation.ConstraintValidator#isValid(java.lang.Object,
	 * javax.validation.ConstraintValidatorContext)
	 */
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		if (RegExValidateUtils.isMobile(mobile)) {
			return true;
		} else {
			return false;
		}
	}

}
