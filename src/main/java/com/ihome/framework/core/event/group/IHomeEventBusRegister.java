/**
 * 
 */
package com.ihome.framework.core.event.group;

import com.ihome.framework.core.event.IHomeEventBusHandler.EventBusWorkHandler;
import com.ihome.framework.core.event.IHomeEventBusHandler.EventDto;

/**
 * @author zww
 *
 */
public interface IHomeEventBusRegister<E extends EventDto> {
    

    public IHomeEventBusGroupRegister<E> handleEventsWith(EventBusWorkHandler<E>... handlers);
    
    public IHomeEventBusGroupRegister<E> handleEventsWithPool(EventBusWorkHandler<E>... handlers);
    
    public IHomeEventBusGroupRegister<E> handleEventsWithPool(Class<? extends EventBusWorkHandler<E>> clz,Integer consumerSize);
}
