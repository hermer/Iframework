/**
 * 
 */
package com.ihome.framework.core.event.command;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.ihome.framework.core.event.IHomeEventBusHandler;
import com.ihome.framework.core.event.IHomeEventConsumer;
import com.ihome.framework.core.event.command.CommandEventBusSpringBeanRegister.CommandEvent;
import com.ihome.framework.core.event.command.buffer.CommandBuffer.CommandDto;
import com.ihome.framework.core.event.command.handler.SimpleCommandEventHandler;

/**
 * @author zww
 *
 */
public class SimpleCommandEventBusHandler<C extends CommandDto> extends IHomeEventBusHandler<CommandEvent<C>>{
    
    public SimpleCommandEventBusHandler(){
        super(DEFAULT_BUFFER_SIZE*8);
    }
    
    private SimpleCommandEventHandler<C> simpleCommandEventHandler;

    /* (non-Javadoc)
     * @see com.ihome.framework.core.event.IHomeEventConsumer#buildEventsWith(com.ihome.framework.core.event.IHomeEventConsumer, org.springframework.context.ApplicationContext)
     */
    @Override
    public void buildEventsWith(IHomeEventConsumer<CommandEvent<C>> consumer, ApplicationContext applicationContext) {
        consumer.handleEventsWith(simpleCommandEventHandler);
        consumer.build(); 
    }
    
    /* (non-Javadoc)
     * @see com.ihome.framework.core.event.IHomeEventBusHandler#newInstance()
     */
    @Override
    public CommandEvent<C> newInstance() {
        return new CommandEvent<C>();
    }
    
    /**
     * @param simpleCommandEventHandler the simpleCommandEventHandler to set
     */
    @Autowired
    public void setSimpleCommandEventHandler(SimpleCommandEventHandler<C> simpleCommandEventHandler) {
        this.simpleCommandEventHandler = simpleCommandEventHandler;
    }
    
   
    

}
