/**
 * 
 */
package com.ihome.framework.core.event;

import com.ihome.framework.core.event.IHomeEventBusHandler.EventDto;

/**
 * @author zww
 *
 */
public interface IHomeEventTranslator<E extends EventDto> {
    
    public void translateTo(E e);

}
