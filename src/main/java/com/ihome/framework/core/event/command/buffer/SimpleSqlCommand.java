/**
 * 
 */
package com.ihome.framework.core.event.command.buffer;

import com.ihome.framework.core.event.command.buffer.CommandBuffer.SqlCommandDto;

/**
 * @author zww
 *
 */
public class SimpleSqlCommand<T extends SqlCommandDto> extends SqlCommandDto{
    
    private Class<T> clz;
    
    /**
     * @param requestId
     */
    public SimpleSqlCommand(String requestId,Class<T> clz) {
        super(requestId);
        this.clz = clz;
    }

    /* (non-Javadoc)
     * @see com.ihome.framework.core.event.command.buffer.CommandBuffer.SqlCommandDto#toSql()
     */
    @Override
    public String toSql() {
        return null;
    }
    
    public Class<T> getSimpleClass(){
        return clz;
    }
    

}
