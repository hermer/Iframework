/**
 * 
 */
package com.ihome.framework.core.event.command.lua.script;

import java.util.Arrays;

import javax.annotation.Resource;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import com.ihome.framework.core.cache.redis.IHomeRedisClient;

/**
 * @author zww
 *
 */
@Component
public class DuplicateCheckLuaCommandScript implements InitializingBean{

    @Resource
    private IHomeRedisClient homeRedisClient;

    private String SHA_CODE = null;



    public Object execute(String[] keys,String... args){
        return homeRedisClient.evalsha(SHA_CODE,Arrays.asList(keys), Arrays.asList(args));
    }

    /* (non-Javadoc)
     * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
     */
    @Override
    public void afterPropertiesSet() throws Exception {
//        SHA_CODE = homeRedisClient.scriptLoad(toLuaScript(), getClass().getSimpleName());
    }


    /* (non-Javadoc)
     * @see com.ihome.framework.core.event.command.lua.LuaCommandUploadProcessor.LuaCommandDto#toLuaScript()
     */
    public String toLuaScript() {
        return null;
//        return new StringBuffer("local KEY = KEYS[1];")
//                .append("local SHA_CODE = KEYS[2];")
//                .append("if(redis.call('hsetnx',KEY,'duplicate',KEY) == 1) then")
//                .append("   return 1;")
//                .append("else")
//                .append("   return 0;")
//                .append("end").toString();
//        return "return redis.call('evalsha','232fd51614574cf0867b83d384a5e898cfd24e5a',0);";
    }

}
