/**
 * 
 */
package com.ihome.framework.core.event;

import org.springframework.context.ApplicationContext;

import com.ihome.framework.core.event.IHomeEventBusHandler.EventBusWorkHandler;
import com.ihome.framework.core.event.IHomeEventBusHandler.EventDto;
import com.ihome.framework.core.event.group.IHomeEventBusRegister;
import com.lmax.disruptor.dsl.Disruptor;

/**
 * @author zww
 *
 */
public interface IHomeEventConsumer<E extends EventDto> extends IHomeEventBusRegister<E> {

    void build();

    @Deprecated
    Disruptor<E> getDisruptor();

    IHomeEventConsumer<E> afterHandleEventsWith(EventBusWorkHandler<E>[] afterHandlers,EventBusWorkHandler<E>... handlers);

    IHomeEventConsumer<E> afterHandleEventsWithPool(EventBusWorkHandler<E>[] afterHandlers,EventBusWorkHandler<E>... handlers);


    void buildEventsWith(IHomeEventConsumer<E> consumer,ApplicationContext applicationContext);



}
