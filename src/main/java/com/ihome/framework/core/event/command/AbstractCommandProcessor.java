/**
 * 
 */
package com.ihome.framework.core.event.command;


import org.springframework.beans.factory.annotation.Autowired;

import com.ihome.framework.core.event.IHomeEventProducer;
import com.ihome.framework.core.event.IHomeEventTranslator;
import com.ihome.framework.core.event.command.CommandEventBusSpringBeanRegister.CommandEvent;
import com.ihome.framework.core.event.command.buffer.CommandBuffer.CommandDto;

/**
 * @author zww
 *
 */
public abstract class AbstractCommandProcessor<C extends CommandDto> implements CommandProcessor<C>{

    private IHomeEventProducer<CommandEvent<C>>  commandEventProducer;

    /* (non-Javadoc)
     * @see com.ihome.framework.core.event.command.CommandProcessor#process(com.ihome.framework.core.event.command.buffer.CommandBuffer.CommandDto)
     */
    @Override
    public void process(final C command) {
        commandEventProducer.publishEvent(new IHomeEventTranslator<CommandEvent<C>>() {
            @Override
            public void translateTo(CommandEvent<C> e) {
                e.setCommandDto(command);
            }
        });
    }
    
    /**
     * @param commandEventProducer the commandEventProducer to set
     */
    @Autowired(required=false)
    public void setCommandEventProducer(IHomeEventProducer<CommandEvent<C>> commandEventProducer) {
        this.commandEventProducer = commandEventProducer;
    }
}
