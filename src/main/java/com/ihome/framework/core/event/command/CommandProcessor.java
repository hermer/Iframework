/**
 * 
 */
package com.ihome.framework.core.event.command;

import com.ihome.framework.core.event.command.buffer.CommandBuffer.CommandDto;

/**
 * @author zww
 *
 */
public interface CommandProcessor<T extends CommandDto> {
    
    void process(T command);

}
