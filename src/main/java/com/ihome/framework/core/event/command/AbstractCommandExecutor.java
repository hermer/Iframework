/**
 * 
 */
package com.ihome.framework.core.event.command;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.jdbc.core.JdbcTemplate;

import com.ihome.framework.core.event.command.buffer.CommandBuffer;
import com.ihome.framework.core.event.command.buffer.CommandBuffer.SqlCommandDto;

/**
 * @author zww
 *
 */
public abstract class AbstractCommandExecutor<B extends CommandBuffer> implements InitializingBean{
    
    @Resource
    private transient DataSource dataSource;

    protected transient JdbcTemplate template;
    

    /* (non-Javadoc)
     * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        template = new JdbcTemplate(dataSource);
    }
    
    
    public String[] toSqlArrays(B buffer){
        List<String> list = new ArrayList<String>();
        List<SqlCommandDto> commands =  buffer.get();
        for (SqlCommandDto commandDto : commands) {
            list.add(commandDto.toSql());
        }
        return list.toArray(new String[]{});
    }
    
    
    

}
