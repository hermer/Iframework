/**
 * 
 */
package com.ihome.framework.core.event.command;

import com.ihome.framework.core.event.command.buffer.AccountWaterInsertCommandBuffer;
import com.ihome.framework.core.event.command.buffer.CommandBuffer;
import com.ihome.framework.core.event.command.buffer.CommandBuffer.CommandDto;

/**
 * @author zww
 *
 */
public interface CommandExecutor<D extends CommandDto,T extends CommandBuffer<D>> {
    
    void execute(T commandBuffer);

}
