/**
 * 
 */
package com.ihome.framework.core.event.group;

import com.ihome.framework.core.event.IHomeEventBusHandler.EventBusWorkHandler;
import com.ihome.framework.core.event.IHomeEventBusHandler.EventDto;

/**
 * @author zww
 *
 */
public interface IHomeEventBusGroupRegister<E extends EventDto> extends IHomeEventBusRegister<E>{
    
    public IHomeEventBusGroupRegister<E> thenGroupDoneWith(EventBusWorkHandler<E>... handlers);
    
    public IHomeEventBusGroupRegister<E> thenGroupDoneWithPool(EventBusWorkHandler<E>... handlers);
    
    public IHomeEventBusGroupRegister<E> thenGroupDoneWithPool(Class<? extends EventBusWorkHandler<E>> clz,Integer consumerSize);

}
