/**
 * 
 */
package com.ihome.framework.core.event;

import com.ihome.framework.core.event.IHomeEventBusHandler.EventDto;

/**
 * @author zww
 *
 */
public interface IHomeEventProducer<E extends EventDto> {
    public void publishEvent(IHomeEventTranslator<E> translator);
}
