/**
 * 
 */
package com.ihome.framework.core.event.command.buffer.mybatis;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.binding.MapperMethod;
import org.apache.ibatis.binding.MapperProxy;
import org.apache.ibatis.binding.MapperProxyFactory;
import org.apache.ibatis.binding.MapperMethod.MethodSignature;
import org.apache.ibatis.binding.MapperMethod.ParamMap;
import org.apache.ibatis.binding.MapperMethod.SqlCommand;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.jdbc.SqlBuilder;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.mapping.ParameterMode;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.SystemMetaObject;
import org.apache.ibatis.reflection.factory.DefaultObjectFactory;
import org.apache.ibatis.reflection.factory.ObjectFactory;
import org.apache.ibatis.reflection.property.PropertyTokenizer;
import org.apache.ibatis.reflection.wrapper.DefaultObjectWrapperFactory;
import org.apache.ibatis.reflection.wrapper.ObjectWrapperFactory;
import org.apache.ibatis.scripting.xmltags.ForEachSqlNode;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandlerRegistry;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.ihome.framework.core.event.IHomeEventBusHandler;
import com.ihome.framework.core.event.IHomeEventBusHandler.EventDto;
import com.ihome.framework.core.event.command.buffer.SimpleSqlCommand;
import com.ihome.framework.core.event.command.buffer.CommandBuffer.SqlCommandDto;

/**
 * @author zww
 *
 */
public abstract class DaoMapperProxy implements InitializingBean{

    @Autowired(required=false)
    private SqlSessionFactory sqlSessionFactory;

    private Configuration configuration;

    private static final ObjectFactory DEFAULT_OBJECT_FACTORY = new DefaultObjectFactory();
    private static final ObjectWrapperFactory DEFAULT_OBJECT_WRAPPER_FACTORY = new DefaultObjectWrapperFactory();

    @Deprecated
    private static final ConcurrentMap<Object, Pair<Long, Long>> RESPONSE_MAP = new ConcurrentHashMap<>();
    

    public DaoMapperProxy(){

    }

    /* (non-Javadoc)
     * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        if(sqlSessionFactory != null)
            configuration = sqlSessionFactory.getConfiguration();
    }


    public <T extends SqlCommandDto> void invoke(Object proxy,String methodName,Class<T> clz,Object... args) throws Exception{
        EventDto event = IHomeEventBusHandler.EventBusWorkHandler.LOCAL_EVENT.get();
        Assert.notNull(sqlSessionFactory,"not mybatis session factory on ioc context! check config...");
        Assert.notNull(proxy,"proxy is null...");
        

        final String sql = getOrigSqlFormat(proxy, methodName, args);

        SimpleSqlCommand<T> sqlCommand = new SimpleSqlCommand<T>(event.getRequestId(),clz){

            private static final long serialVersionUID = 1L;

            public String toSql() {
                return sql;
            };
        };
        event.addCommand(sqlCommand);
    }


    protected String getOrigSqlFormat(Object proxy,String methodName,Object... args) throws Exception{
        String sql = getOrigSql(proxy, methodName, args);
        if(StringUtils.isNotBlank(sql)){
            sql = sql.replaceAll("\n\t", "").replaceAll("\\s+", " ");//.replaceAll("\\s+,", ",").replaceAll("\\(\\s+", "(").replaceAll("\\s+\\)", ")");
        }
        return sql;
        
    }
    
    

    protected String getOrigSql(Object proxy,String methodName,Object... args) throws Exception{
        return getMapperSql(proxy, methodName, args);
    }





    protected  MetaObject forObject(Object object) {
        return MetaObject.forObject(object, DEFAULT_OBJECT_FACTORY, DEFAULT_OBJECT_WRAPPER_FACTORY);
    }


    protected String getMapperSql(Object mapper, String methodName, Object... args) {
        MetaObject metaObject = forObject(mapper);
        SqlSession session = (SqlSession) metaObject.getValue("h.sqlSession");
        Class mapperInterface = (Class) metaObject.getValue("h.mapperInterface");
        String fullMethodName = mapperInterface.getCanonicalName() + "." + methodName;
        if (args == null || args.length == 0) {
            return getNamespaceSql(fullMethodName, null);
        } else {
            return getMapperSql(mapperInterface, methodName, args);
        }
    }



    protected String getMapperSql(String fullMapperMethodName, Object... args) {


        if (args == null || args.length == 0) {
            return getNamespaceSql(fullMapperMethodName, null);
        }
        String methodName = fullMapperMethodName.substring(fullMapperMethodName.lastIndexOf('.') + 1);
        Class mapperInterface = null;
        try {
            mapperInterface = Class.forName(fullMapperMethodName.substring(0, fullMapperMethodName.lastIndexOf('.')));
            return getMapperSql(mapperInterface, methodName, args);
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException("param " + fullMapperMethodName + " is invalid...");
        }
    }


    protected  String getMapperSql(Class mapperInterface, String methodName, Object... args) {
        String fullMapperMethodName = mapperInterface.getCanonicalName() + "." + methodName;
        if (args == null || args.length == 0) {
            return getNamespaceSql(fullMapperMethodName, null);
        }
        Method method = getDeclaredMethods(mapperInterface, methodName);
        Map params = new HashMap();
        final Class<?>[] argTypes = method.getParameterTypes();
        for (int i = 0; i < argTypes.length; i++) {
            if (!RowBounds.class.isAssignableFrom(argTypes[i]) && !ResultHandler.class.isAssignableFrom(argTypes[i])) {
                String paramName = "param" + String.valueOf(params.size() + 1);
                paramName = getParamNameFromAnnotation(method, i, paramName);
                params.put(paramName, i >= args.length ? null : args[i]);
            }
        }
        if (args != null && args.length == 1) {
            Object _params = wrapCollection(args[0]);
            if (_params instanceof Map) {
                params.putAll((Map) _params);
            }
        }
        return getNamespaceSql(fullMapperMethodName, params);
    }


    protected  String getNamespaceSql(SqlSession session, String namespace) {
        return getNamespaceSql(namespace, null);
    }


    protected  String getNamespaceSql(String namespace, Object params) {
        params = wrapCollection(params);
        MappedStatement mappedStatement = configuration.getMappedStatement(namespace);
        TypeHandlerRegistry typeHandlerRegistry = mappedStatement.getConfiguration().getTypeHandlerRegistry();
        BoundSql boundSql = mappedStatement.getBoundSql(params);
        List<ParameterMapping> parameterMappings = boundSql.getParameterMappings();
        String sql = boundSql.getSql();
        if (parameterMappings != null) {
            for (int i = 0; i < parameterMappings.size(); i++) {
                ParameterMapping parameterMapping = parameterMappings.get(i);
                if (parameterMapping.getMode() != ParameterMode.OUT) {
                    Object value;
                    String propertyName = parameterMapping.getProperty();
                    if (boundSql.hasAdditionalParameter(propertyName)) {
                        value = boundSql.getAdditionalParameter(propertyName);
                    } else if (params == null) {
                        value = null;
                    } else if (typeHandlerRegistry.hasTypeHandler(params.getClass())) {
                        value = params;
                    } else {
                        MetaObject metaObject = configuration.newMetaObject(params);
                        value = metaObject.getValue(propertyName);
                    }
                    JdbcType jdbcType = parameterMapping.getJdbcType();
                    if (value == null && jdbcType == null)
                        jdbcType = configuration.getJdbcTypeForNull();
                    sql = replaceParameter(sql, value, jdbcType, parameterMapping.getJavaType());
                }
            }
        }
        return sql;
    }

    private  String replaceParameter(String sql, Object value, JdbcType jdbcType, Class javaType) {
        String strValue = null;
        if(value != null){
            strValue = String.valueOf(value);
            if (jdbcType != null) {
                switch (jdbcType) {
                case BIT:
                case TINYINT:
                case SMALLINT:
                case INTEGER:
                case BIGINT:
                case FLOAT:
                case REAL:
                case DOUBLE:
                case NUMERIC:
                case DECIMAL:
                    break;
                case DATE:
                case TIME:
                case TIMESTAMP:
                default:
                    if(value instanceof Date){
                        strValue=com.ihome.framework.core.utils.DateFormatUtils.format((Date)value);
                    }
                    strValue = "'" + strValue + "'";
                }
            } else if (Number.class.isAssignableFrom(javaType)) {
                // 不加单引号
            } else {
                strValue = "'" + strValue + "'";
            }
        }else{
            strValue = "NULL";
        }
        return sql.replaceFirst("\\?", strValue);
    }



    private  Method getDeclaredMethods(Class clazz, String methodName) {
        Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            if (method.getName().equals(methodName)) {
                return method;
            }
        }
        throw new IllegalArgumentException("method " + methodName + "not exist!");
    }



    private  String getParamNameFromAnnotation(Method method, int i, String paramName) {
        final Object[] paramAnnos = method.getParameterAnnotations()[i];
        for (Object paramAnno : paramAnnos) {
            if (paramAnno instanceof Param) {
                paramName = ((Param) paramAnno).value();
            }
        }
        return paramName;
    }

    private  Object wrapCollection(final Object object) {
        if (object instanceof List) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("list", object);
            return map;
        } else if (object != null && object.getClass().isArray()) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("array", object);
            return map;
        }
        return object;
    }


}

