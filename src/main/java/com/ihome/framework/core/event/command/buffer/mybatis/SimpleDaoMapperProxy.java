/**
 * 
 */
package com.ihome.framework.core.event.command.buffer.mybatis;

import org.springframework.stereotype.Component;

import com.ihome.framework.core.event.IHomeEventBusHandler.EventDto;

/**
 * @author zww
 *
 */
@Component
public class SimpleDaoMapperProxy extends DaoMapperProxy{

    /**
     * @param event
     */
    public SimpleDaoMapperProxy() {
    }

}
