package com.ihome.framework.core.sftp;

import com.jcraft.jsch.Channel;

/**
 * use to get Channel
 * 
 * @author liuliang
 *
 */
public interface IChannelFactory {

	/**
	 * 
	 * @return connected channel;otherwise null if get failure.
	 */
	// public Channel openChannel();

	/**
	 *
	 * @return connected channel;otherwise null if get failure.
	 */
	public Channel openChannelWithNewSession();

	/**
	 * open channel with config
	 * 
	 * @param sftpConfig
	 *            sftpconfig
	 * @return channel
	 */
	public Channel openChannelWithNewSession(SFtpConfig sftpConfig);

	/**
	 * close channel resources.
	 */
	// public void close(Channel channel);

	/**
	 * close channel and session resources.
	 */
	public void closeChannelAndSession(Channel channel);

	/**
	 * destory the session
	 */
	public void destory();

	/**
	 * init the sftp params
	 */
	public void init();
}
