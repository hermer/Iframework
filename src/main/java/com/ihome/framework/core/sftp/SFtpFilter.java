package com.ihome.framework.core.sftp;

import com.jcraft.jsch.ChannelSftp;

public abstract interface SFtpFilter {
	public abstract <E> E filter(ChannelSftp channel, E e);
}