package com.ihome.framework.core.sftp;

import com.jcraft.jsch.ChannelSftp;

public abstract interface SFtpFileValidator {
	public abstract <E> boolean validate(ChannelSftp channel, E e);
}