package com.ihome.framework.core.sftp;

/**
 * sftp服务器配置
 * 
 *
 */

public class SFtpConfig {

	protected String host;
	protected int port;
	protected String username;
	protected String password;
	protected int timeout;
	protected String privatekey;
	protected String privatekeyValue;
	protected String privatekeypassphrase;

	public String getHost() {
		return host;
	}

	public int getPort() {
		return port;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public int getTimeout() {
		return timeout;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public String getPrivatekey() {
		return privatekey;
	}

	public void setPrivatekey(String privatekey) {
		this.privatekey = privatekey;
	}

	public String getPrivatekeyValue() {
		return privatekeyValue;
	}

	public void setPrivatekeyValue(String privatekeyValue) {
		this.privatekeyValue = privatekeyValue;
	}

	public String getPrivatekeypassphrase() {
		return privatekeypassphrase;
	}

	public void setPrivatekeypassphrase(String privatekeypassphrase) {
		this.privatekeypassphrase = privatekeypassphrase;
	}
}
