package com.ihome.framework.core.dubbo.hystrix;

public class HystrixConfig {

	// 熔断器是否开启，默认开启
	private boolean enabled;
	// 熔断器错误比率阈值
	private int errorThresholdPercentage;
	// 表示请求数至少达到多大才进行熔断计算
	private int requestVolumeThreshold;
	// 半开的触发试探休眠时间
	private int sleepWindowInMilliseconds;
	// 核心线程数，maxSize也是该值
	private int coreSize;
	// 空闲线程保活时间
	private int keepAliveTime;
	// 最大队列大小，如果-1则会使用交换队列
	private int maxQueueSize;
	// 当等待队列多大的时候，将会执行决绝策略
	private int queueSizeRejectionThreashold;
	// 超时是否开启
	private boolean timeoutEnabled;
	// 执行线程的超时时间
	private int timeoutInMilliseconds;

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public int getErrorThresholdPercentage() {
		return errorThresholdPercentage;
	}

	public void setErrorThresholdPercentage(int errorThresholdPercentage) {
		this.errorThresholdPercentage = errorThresholdPercentage;
	}

	public int getRequestVolumeThreshold() {
		return requestVolumeThreshold;
	}

	public void setRequestVolumeThreshold(int requestVolumeThreshold) {
		this.requestVolumeThreshold = requestVolumeThreshold;
	}

	public int getSleepWindowInMilliseconds() {
		return sleepWindowInMilliseconds;
	}

	public void setSleepWindowInMilliseconds(int sleepWindowInMilliseconds) {
		this.sleepWindowInMilliseconds = sleepWindowInMilliseconds;
	}

	public int getCoreSize() {
		return coreSize;
	}

	public void setCoreSize(int coreSize) {
		this.coreSize = coreSize;
	}

	public int getKeepAliveTime() {
		return keepAliveTime;
	}

	public void setKeepAliveTime(int keepAliveTime) {
		this.keepAliveTime = keepAliveTime;
	}

	public int getMaxQueueSize() {
		return maxQueueSize;
	}

	public void setMaxQueueSize(int maxQueueSize) {
		this.maxQueueSize = maxQueueSize;
	}

	public int getQueueSizeRejectionThreashold() {
		return queueSizeRejectionThreashold;
	}

	public void setQueueSizeRejectionThreashold(int queueSizeRejectionThreashold) {
		this.queueSizeRejectionThreashold = queueSizeRejectionThreashold;
	}

	public boolean isTimeoutEnabled() {
		return timeoutEnabled;
	}

	public void setTimeoutEnabled(boolean timeoutEnabled) {
		this.timeoutEnabled = timeoutEnabled;
	}

	public int getTimeoutInMilliseconds() {
		return timeoutInMilliseconds;
	}

	public void setTimeoutInMilliseconds(int timeoutInMilliseconds) {
		this.timeoutInMilliseconds = timeoutInMilliseconds;
	}

}
