package com.ihome.framework.core.dubbo.hystrix;

import com.alibaba.dubbo.common.Constants;
import com.alibaba.dubbo.common.extension.Activate;
import com.alibaba.dubbo.rpc.Filter;
import com.alibaba.dubbo.rpc.Invocation;
import com.alibaba.dubbo.rpc.Invoker;
import com.alibaba.dubbo.rpc.Result;
import com.alibaba.dubbo.rpc.RpcContext;
import com.alibaba.dubbo.rpc.RpcException;

@Activate(group = Constants.CONSUMER)
public class HystrixFilter implements Filter {

	private HystrixConfig hystrixConfig;

	public HystrixConfig getHystrixConfig() {
		return hystrixConfig;
	}

	public void setHystrixConfig(HystrixConfig hystrixConfig) {
		this.hystrixConfig = hystrixConfig;
	}

	public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
		// 获取hystrix配置
		if (hystrixConfig != null) {
			DubboHystrixCommand command = new DubboHystrixCommand(invoker, invocation, hystrixConfig,
					RpcContext.getContext());
			return command.execute();
		} else {
			return invoker.invoke(invocation);
		}
	}
}