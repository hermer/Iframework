package com.ihome.framework.core.task;

import java.lang.reflect.Method;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;

import com.alibaba.dubbo.rpc.RpcContext;

/**
 * 调度任务@Schedule切面
 * 
 * @author limeng
 *
 */
@Aspect
public class ScheduleAspect {

	public void doBefore(JoinPoint jp) {
		// 获取Schedule注解
		MethodSignature ms = (MethodSignature) jp.getSignature();
		Method method = ms.getMethod();
		Schedule schedule = method.getAnnotation(Schedule.class);
		if (schedule != null) {
			// 清除Context 异步标志
			RpcContext.getContext().removeAttachment("async");
		}
	}

}
