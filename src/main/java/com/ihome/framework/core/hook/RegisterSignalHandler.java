/**
 * 
 */
package com.ihome.framework.core.hook;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import sun.misc.Signal;
import sun.misc.SignalHandler;

/**
 * @author zww
 *
 */
@SuppressWarnings("restriction")
public class RegisterSignalHandler implements SignalHandler,InitializingBean{
    
    private Logger LOG = LoggerFactory.getLogger(getClass()); 
    
    
    
    public static void listenTo(String name) {
        Signal signal = new Signal(name);
        Signal.handle(signal, new RegisterSignalHandler());
     }
    

    /* (non-Javadoc)
     * @see sun.misc.SignalHandler#handle(sun.misc.Signal)
     */
    @Override
    public void handle(Signal arg0) {
        LOG.debug(String.format("handle Signal[%s]", arg0.getName()));
        System.exit(0);
    }

    /* (non-Javadoc)
     * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
     */
    @Override
    public void afterPropertiesSet() throws Exception {
//        RegisterSignalHandler.listenTo("HUP");
        RegisterSignalHandler.listenTo("INT"); // kill 2
//        RegisterSignalHandler.listenTo("USR1"); //
//        RegisterSignalHandler.listenTo("QUIT");  // kill 3
//        RegisterSignalHandler.listenTo("USR2");
//        RegisterSignalHandler.listenTo("KILL");
        RegisterSignalHandler.listenTo("TERM"); // kill 15
    }

}
