package com.ihome.framework.core.cache.nkv;

import com.ihome.framework.core.exception.PlatformException;
import com.netease.backend.nkv.client.Result.ResultCode;

public class NkvOperateException extends PlatformException {

	private ResultCode resultCode;

	public NkvOperateException(int code, String message, ResultCode resultCode) {
		super(code, message);
		this.resultCode = resultCode;
	}

	public ResultCode getResultCode() {
		return resultCode;
	}

}
