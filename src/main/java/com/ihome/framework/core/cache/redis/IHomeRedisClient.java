package com.ihome.framework.core.cache.redis;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.InitializingBean;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Tuple;
import redis.clients.jedis.params.sortedset.ZAddParams;
import redis.clients.jedis.params.sortedset.ZIncrByParams;

public class IHomeRedisClient implements InitializingBean{

    // 非redis集群客户端
    private JedisPool jedisPool;
    // redis集群客户端
    private JedisCluster jedisCluster;

    // 是否是redis集群
    private boolean isCluster;
    // 产品命名空间
    private String namespace;

    private JedisPoolConfig poolConfig;


    public IHomeRedisClient(String address, String namespace, String password, boolean isCluster, int connectionTimeout,
            int soTimeout, int maxAttempts,JedisPoolConfig jedisPoolConfig) {
        this.isCluster = isCluster;
        this.namespace = namespace;
        this.poolConfig = jedisPoolConfig;
        // 配置集群客户端
        if (isCluster) {
            Set<HostAndPort> jedisClusterNodes = new HashSet<HostAndPort>();
            String[] hostsAndPorts = address.split(";");
            for (String str : hostsAndPorts) {
                String host = str.split(":")[0];
                int port = Integer.parseInt(str.split(":")[1]);
                jedisClusterNodes.add(new HostAndPort(host, port));
            }
            if (password != null) {
                jedisCluster = new JedisCluster(jedisClusterNodes, connectionTimeout, soTimeout, maxAttempts, password,
                        jedisPoolConfig);
            } else {
                jedisCluster = new JedisCluster(jedisClusterNodes, connectionTimeout, soTimeout, maxAttempts,
                        jedisPoolConfig);
            }
        }
        // 配置非集群客户端
        else {
            String[] hostAndPort = address.split(":");
            String host = hostAndPort[0];
            int port = Integer.parseInt(hostAndPort[1]);
            jedisPool = new JedisPool(jedisPoolConfig, host, port, connectionTimeout, password);
        }
    }

    /* (non-Javadoc)
     * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        if(poolConfig != null){
            List<Jedis> minIdleJedisList = new ArrayList<Jedis>(poolConfig.getMinIdle());
            try{
                for (int i = 0; i < poolConfig.getMinIdle(); i++) {
                    minIdleJedisList.add(getResource());
                }
                for (Jedis jedis : minIdleJedisList) {
                    jedis.ping();
                    returnResource(jedis);
                }
            }finally {
                minIdleJedisList.clear();
            }
        } 
    }



    protected void returnResource(Jedis resource){
        jedisPool.returnResource(resource);
    }

    protected Jedis getResource(){
        return jedisPool.getResource();
    }

    public void scriptFlush(String name){
        if (isCluster) {
            jedisCluster.scriptFlush(name.getBytes());
        } 
    }

    public String scriptLoad(String script,String name){
        if (isCluster) {
            return jedisCluster.scriptLoad(script, name);
        }else{
            Jedis jedis = getResource();
            try{
                return jedis.scriptLoad(script);
            }finally {
                returnResource(jedis);
            }
        }
    }



    /**
     * @return the namespace
     */
    public String getNamespace() {
        return namespace;
    }


    public <T> T evalsha(String sha,List<String> keys,List<String> args){
        if (isCluster) {
            return (T) jedisCluster.evalsha(sha, keys, args);
        } else {
            Jedis jedis = getResource();
            try{
                return (T) jedis.evalsha(sha, keys, args);
            }finally {
                returnResource(jedis); 
            }
        }
    }





    /**
     * Set the string value as value of the key. The string can't be longer than
     * 1073741824 bytes (1 GB).
     * 
     * @param key
     * @param value
     * @return Status code reply
     */
    public String set(String key, String value) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.set(key, value);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.set(key, value);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Set the string value as value of the key. The string can't be longer than
     * 1073741824 bytes (1 GB).
     * 
     * @param key
     * @param value
     * @param nxxx
     *            NX|XX, NX -- Only set the key if it does not already exist. XX
     *            -- Only set the key if it already exist.
     * @param expx
     *            EX|PX, expire time units: EX = seconds; PX = milliseconds
     * @param time
     *            expire time in the units of <code>expx</code>
     * @return Status code reply
     */
    public String set(String key, final String value, final String nxxx, final String expx, final long time) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.set(key, value, nxxx, expx, time);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.set(key, value, nxxx, expx, time);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Get the value of the specified key. If the key does not exist null is
     * returned. If the value stored at key is not a string an error is returned
     * because GET can only handle string values.
     * <p>
     * Time complexity: O(1)
     * 
     * @param key
     * @return Bulk reply
     */
    public String get(String key) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.get(key);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.get(key);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Test if the specified key exists. The command returns the number of keys
     * existed Time complexity: O(N)
     * 
     * @param keys
     * @return Integer reply, specifically: an integer greater than 0 if one or
     *         more keys were removed 0 if none of the specified key existed
     */
    public Long exists(final String... keys) {
        String[] newKeys = transformKeys(keys);
        if (isCluster) {
            return jedisCluster.exists(newKeys);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.exists(keys);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Test if the specified key exists. The command returns "1" if the key
     * exists, otherwise "0" is returned. Note that even keys set with an empty
     * string as value will return "1". Time complexity: O(1)
     * 
     * @param key
     * @return Boolean reply, true if the key exists, otherwise false
     */
    public Boolean exists(String key) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.exists(key);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.exists(key);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Remove the specified keys. If a given key does not exist no operation is
     * performed for this key. The command returns the number of keys removed.
     * Time complexity: O(1)
     * 
     * @param keys
     * @return Integer reply, specifically: an integer greater than 0 if one or
     *         more keys were removed 0 if none of the specified key existed
     */
    public Long del(final String... keys) {
        String[] newKeys = transformKeys(keys);
        if (isCluster) {
            return jedisCluster.del(newKeys);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.del(newKeys);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    public Long del(String key) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.del(key);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.del(key);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Return the type of the value stored at key in form of a string. The type
     * can be one of "none", "string", "list", "set". "none" is returned if the
     * key does not exist. Time complexity: O(1)
     * 
     * @param key
     * @return Status code reply, specifically: "none" if the key does not exist
     *         "string" if the key contains a String value "list" if the key
     *         contains a List value "set" if the key contains a Set value
     *         "zset" if the key contains a Sorted Set value "hash" if the key
     *         contains a Hash value
     */
    public String type(String key) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.type(key);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.type(key);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Atomically renames the key oldkey to newkey. If the source and
     * destination name are the same an error is returned. If newkey already
     * exists it is overwritten.
     * <p>
     * Time complexity: O(1)
     * 
     * @param oldkey
     * @param newkey
     * @return Status code repy
     */
    public String rename(String oldkey, String newkey) {
        oldkey = namespace + oldkey;
        newkey = namespace + newkey;
        if (isCluster) {
            return jedisCluster.rename(oldkey, newkey);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.rename(oldkey, newkey);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Rename oldkey into newkey but fails if the destination key newkey already
     * exists.
     * <p>
     * Time complexity: O(1)
     * 
     * @param oldkey
     * @param newkey
     * @return Integer reply, specifically: 1 if the key was renamed 0 if the
     *         target key already exist
     */
    public Long renamenx(String oldkey, String newkey) {
        oldkey = namespace + oldkey;
        newkey = namespace + newkey;
        if (isCluster) {
            return jedisCluster.renamenx(oldkey, newkey);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.renamenx(oldkey, newkey);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Set a timeout on the specified key. After the timeout the key will be
     * automatically deleted by the server. A key with an associated timeout is
     * said to be volatile in Redis terminology.
     * <p>
     * Voltile keys are stored on disk like the other keys, the timeout is
     * persistent too like all the other aspects of the dataset. Saving a
     * dataset containing expires and stopping the server does not stop the flow
     * of time as Redis stores on disk the time when the key will no longer be
     * available as Unix time, and not the remaining seconds.
     * <p>
     * Since Redis 2.1.3 you can update the value of the timeout of a key
     * already having an expire set. It is also possible to undo the expire at
     * all turning the key into a normal key using the {@link #persist(String)
     * PERSIST} command.
     * <p>
     * Time complexity: O(1)
     * 
     * @see <a href="http://code.google.com/p/redis/wiki/ExpireCommand">
     *      ExpireCommand</a>
     * @param key
     * @param seconds
     * @return Integer reply, specifically: 1: the timeout was set. 0: the
     *         timeout was not set since the key already has an associated
     *         timeout (this may happen only in Redis versions &lt; 2.1.3, Redis
     *         &gt;= 2.1.3 will happily update the timeout), or the key does not
     *         exist.
     */
    public Long expire(String key, final int seconds) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.expire(key, seconds);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.expire(key, seconds);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * EXPIREAT works exctly like {@link #expire(String, int) EXPIRE} but
     * instead to get the number of seconds representing the Time To Live of the
     * key as a second argument (that is a relative way of specifing the TTL),
     * it takes an absolute one in the form of a UNIX timestamp (Number of
     * seconds elapsed since 1 Gen 1970).
     * <p>
     * EXPIREAT was introduced in order to implement the Append Only File
     * persistence mode so that EXPIRE commands are automatically translated
     * into EXPIREAT commands for the append only file. Of course EXPIREAT can
     * also used by programmers that need a way to simply specify that a given
     * key should expire at a given time in the future.
     * <p>
     * Since Redis 2.1.3 you can update the value of the timeout of a key
     * already having an expire set. It is also possible to undo the expire at
     * all turning the key into a normal key using the {@link #persist(String)
     * PERSIST} command.
     * <p>
     * Time complexity: O(1)
     * 
     * @see <a href="http://code.google.com/p/redis/wiki/ExpireCommand">
     *      ExpireCommand</a>
     * @param key
     * @param unixTime
     * @return Integer reply, specifically: 1: the timeout was set. 0: the
     *         timeout was not set since the key already has an associated
     *         timeout (this may happen only in Redis versions &lt; 2.1.3, Redis
     *         &gt;= 2.1.3 will happily update the timeout), or the key does not
     *         exist.
     */
    public Long expireAt(String key, final long unixTime) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.expireAt(key, unixTime);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.expireAt(key, unixTime);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * The TTL command returns the remaining time to live in seconds of a key
     * that has an {@link #expire(String, int) EXPIRE} set. This introspection
     * capability allows a Redis client to check how many seconds a given key
     * will continue to be part of the dataset.
     * 
     * @param key
     * @return Integer reply, returns the remaining time to live in seconds of a
     *         key that has an EXPIRE. In Redis 2.6 or older, if the Key does
     *         not exists or does not have an associated expire, -1 is returned.
     *         In Redis 2.8 or newer, if the Key does not have an associated
     *         expire, -1 is returned or if the Key does not exists, -2 is
     *         returned.
     */
    public Long ttl(String key) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.ttl(key);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.ttl(key);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Move the specified key from the currently selected DB to the specified
     * destination DB. Note that this command returns 1 only if the key was
     * successfully moved, and 0 if the target key was already there or if the
     * source key was not found at all, so it is possible to use MOVE as a
     * locking primitive.
     * 
     * @param key
     * @param dbIndex
     * @return Integer reply, specifically: 1 if the key was moved 0 if the key
     *         was not moved because already present on the target DB or was not
     *         found in the current DB.
     */
    public Long move(String key, final int dbIndex) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.move(key, dbIndex);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.move(key, dbIndex);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * GETSET is an atomic set this value and return the old value command. Set
     * key to the string value and return the old value stored at key. The
     * string can't be longer than 1073741824 bytes (1 GB).
     * <p>
     * Time complexity: O(1)
     * 
     * @param key
     * @param value
     * @return Bulk reply
     */
    public String getSet(String key, final String value) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.getSet(key, value);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.getSet(key, value);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Get the values of all the specified keys. If one or more keys dont exist
     * or is not of type String, a 'nil' value is returned instead of the value
     * of the specified key, but the operation never fails.
     * <p>
     * Time complexity: O(1) for every key
     * 
     * @param keys
     * @return Multi bulk reply
     */
    public List<String> mget(final String... keys) {
        String[] newkeys = transformKeys(keys);
        if (isCluster) {
            return jedisCluster.mget(newkeys);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.mget(newkeys);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * SETNX works exactly like {@link #set(String, String) SET} with the only
     * difference that if the key already exists no operation is performed.
     * SETNX actually means "SET if Not eXists".
     * <p>
     * Time complexity: O(1)
     * 
     * @param key
     * @param value
     * @return Integer reply, specifically: 1 if the key was set 0 if the key
     *         was not set
     */
    public Long setnx(String key, final String value) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.setnx(key, value);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.setnx(key, value);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * The command is exactly equivalent to the following group of commands:
     * {@link #set(String, String) SET} + {@link #expire(String, int) EXPIRE}.
     * The operation is atomic.
     * <p>
     * Time complexity: O(1)
     * 
     * @param key
     * @param seconds
     * @param value
     * @return Status code reply
     */
    public String setex(String key, final int seconds, final String value) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.setex(key, seconds, value);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.setex(key, seconds, value);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * IDECRBY work just like {@link #decr(String) INCR} but instead to
     * decrement by 1 the decrement is integer.
     * <p>
     * INCR commands are limited to 64 bit signed integers.
     * <p>
     * Note: this is actually a string operation, that is, in Redis there are
     * not "integer" types. Simply the string stored at the key is parsed as a
     * base 10 64 bit signed integer, incremented, and then converted back as a
     * string.
     * <p>
     * Time complexity: O(1)
     * 
     * @see #incr(String)
     * @see #decr(String)
     * @see #incrBy(String, long)
     * @param key
     * @param integer
     * @return Integer reply, this commands will reply with the new value of key
     *         after the increment.
     */
    public Long decrBy(String key, final long integer) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.decrBy(key, integer);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.decrBy(key, integer);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Decrement the number stored at key by one. If the key does not exist or
     * contains a value of a wrong type, set the key to the value of "0" before
     * to perform the decrement operation.
     * <p>
     * INCR commands are limited to 64 bit signed integers.
     * <p>
     * Note: this is actually a string operation, that is, in Redis there are
     * not "integer" types. Simply the string stored at the key is parsed as a
     * base 10 64 bit signed integer, incremented, and then converted back as a
     * string.
     * <p>
     * Time complexity: O(1)
     * 
     * @see #incr(String)
     * @see #incrBy(String, long)
     * @see #decrBy(String, long)
     * @param key
     * @return Integer reply, this commands will reply with the new value of key
     *         after the increment.
     */
    public Long decr(String key) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.decr(key);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.decr(key);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * INCRBY work just like {@link #incr(String) INCR} but instead to increment
     * by 1 the increment is integer.
     * <p>
     * INCR commands are limited to 64 bit signed integers.
     * <p>
     * Note: this is actually a string operation, that is, in Redis there are
     * not "integer" types. Simply the string stored at the key is parsed as a
     * base 10 64 bit signed integer, incremented, and then converted back as a
     * string.
     * <p>
     * Time complexity: O(1)
     * 
     * @see #incr(String)
     * @see #decr(String)
     * @see #decrBy(String, long)
     * @param key
     * @param integer
     * @return Integer reply, this commands will reply with the new value of key
     *         after the increment.
     */
    public Long incrBy(String key, final long integer) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.incrBy(key, integer);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.incrBy(key, integer);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * INCRBYFLOAT
     * <p>
     * INCRBYFLOAT commands are limited to double precision floating point
     * values.
     * <p>
     * Note: this is actually a string operation, that is, in Redis there are
     * not "double" types. Simply the string stored at the key is parsed as a
     * base double precision floating point value, incremented, and then
     * converted back as a string. There is no DECRYBYFLOAT but providing a
     * negative value will work as expected.
     * <p>
     * Time complexity: O(1)
     * 
     * @param key
     * @param value
     * @return Double reply, this commands will reply with the new value of key
     *         after the increment.
     */
    public Double incrByFloat(String key, final double value) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.incrByFloat(key, value);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.incrByFloat(key, value);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Increment the number stored at key by one. If the key does not exist or
     * contains a value of a wrong type, set the key to the value of "0" before
     * to perform the increment operation.
     * <p>
     * INCR commands are limited to 64 bit signed integers.
     * <p>
     * Note: this is actually a string operation, that is, in Redis there are
     * not "integer" types. Simply the string stored at the key is parsed as a
     * base 10 64 bit signed integer, incremented, and then converted back as a
     * string.
     * <p>
     * Time complexity: O(1)
     * 
     * @see #incrBy(String, long)
     * @see #decr(String)
     * @see #decrBy(String, long)
     * @param key
     * @return Integer reply, this commands will reply with the new value of key
     *         after the increment.
     */
    public Long incr(String key) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.incr(key);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.incr(key);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * If the key already exists and is a string, this command appends the
     * provided value at the end of the string. If the key does not exist it is
     * created and set as an empty string, so APPEND will be very similar to SET
     * in this special case.
     * <p>
     * Time complexity: O(1). The amortized time complexity is O(1) assuming the
     * appended value is small and the already present value is of any size,
     * since the dynamic string library used by Redis will double the free space
     * available on every reallocation.
     * 
     * @param key
     * @param value
     * @return Integer reply, specifically the total length of the string after
     *         the append operation.
     */
    public Long append(String key, final String value) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.append(key, value);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.append(key, value);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Return a subset of the string from offset start to offset end (both
     * offsets are inclusive). Negative offsets can be used in order to provide
     * an offset starting from the end of the string. So -1 means the last char,
     * -2 the penultimate and so forth.
     * <p>
     * The function handles out of range requests without raising an error, but
     * just limiting the resulting range to the actual length of the string.
     * <p>
     * Time complexity: O(start+n) (with start being the start index and n the
     * total length of the requested range). Note that the lookup part of this
     * command is O(1) so for small strings this is actually an O(1) command.
     * 
     * @param key
     * @param start
     * @param end
     * @return Bulk reply
     */
    public String substr(String key, final int start, final int end) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.substr(key, start, end);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.substr(key, start, end);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Set the specified hash field to the specified value.
     * <p>
     * If key does not exist, a new key holding a hash is created.
     * <p>
     * <b>Time complexity:</b> O(1)
     * 
     * @param key
     * @param field
     * @param value
     * @return If the field already exists, and the HSET just produced an update
     *         of the value, 0 is returned, otherwise if a new field is created
     *         1 is returned.
     */
    public Long hset(String key, final String field, final String value) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.hset(key, field, value);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.hset(key, field, value);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * If key holds a hash, retrieve the value associated to the specified
     * field.
     * <p>
     * If the field is not found or the key does not exist, a special 'nil'
     * value is returned.
     * <p>
     * <b>Time complexity:</b> O(1)
     * 
     * @param key
     * @param field
     * @return Bulk reply
     */
    public String hget(String key, final String field) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.hget(key, field);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.hget(key, field);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Set the specified hash field to the specified value if the field not
     * exists. <b>Time complexity:</b> O(1)
     * 
     * @param key
     * @param field
     * @param value
     * @return If the field already exists, 0 is returned, otherwise if a new
     *         field is created 1 is returned.
     */
    public Long hsetnx(String key, final String field, final String value) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.hsetnx(key, field, value);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.hsetnx(key, field, value);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Set the respective fields to the respective values. HMSET replaces old
     * values with new values.
     * <p>
     * If key does not exist, a new key holding a hash is created.
     * <p>
     * <b>Time complexity:</b> O(N) (with N being the number of fields)
     * 
     * @param key
     * @param hash
     * @return Return OK or Exception if hash is empty
     */
    public String hmset(String key, final Map<String, String> hash) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.hmset(key, hash);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.hmset(key, hash);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Retrieve the values associated to the specified fields.
     * <p>
     * If some of the specified fields do not exist, nil values are returned.
     * Non existing keys are considered like empty hashes.
     * <p>
     * <b>Time complexity:</b> O(N) (with N being the number of fields)
     * 
     * @param key
     * @param fields
     * @return Multi Bulk Reply specifically a list of all the values associated
     *         with the specified fields, in the same order of the request.
     */
    public List<String> hmget(String key, final String... fields) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.hmget(key, fields);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.hmget(key, fields);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Increment the number stored at field in the hash at key by value. If key
     * does not exist, a new key holding a hash is created. If field does not
     * exist or holds a string, the value is set to 0 before applying the
     * operation. Since the value argument is signed you can use this command to
     * perform both increments and decrements.
     * <p>
     * The range of values supported by HINCRBY is limited to 64 bit signed
     * integers.
     * <p>
     * <b>Time complexity:</b> O(1)
     * 
     * @param key
     * @param field
     * @param value
     * @return Integer reply The new value at field after the increment
     *         operation.
     */
    public Long hincrBy(String key, final String field, final long value) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.hincrBy(key, field, value);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.hincrBy(key, field, value);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Increment the number stored at field in the hash at key by a double
     * precision floating point value. If key does not exist, a new key holding
     * a hash is created. If field does not exist or holds a string, the value
     * is set to 0 before applying the operation. Since the value argument is
     * signed you can use this command to perform both increments and
     * decrements.
     * <p>
     * The range of values supported by HINCRBYFLOAT is limited to double
     * precision floating point values.
     * <p>
     * <b>Time complexity:</b> O(1)
     * 
     * @param key
     * @param field
     * @param value
     * @return Double precision floating point reply The new value at field
     *         after the increment operation.
     */
    public Double hincrByFloat(String key, final String field, final double value) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.hincrByFloat(key, field, value);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.hincrByFloat(key, field, value);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Test for existence of a specified field in a hash. <b>Time
     * complexity:</b> O(1)
     * 
     * @param key
     * @param field
     * @return Return 1 if the hash stored at key contains the specified field.
     *         Return 0 if the key is not found or the field is not present.
     */
    public Boolean hexists(String key, final String field) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.hexists(key, field);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.hexists(key, field);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Remove the specified field from an hash stored at key.
     * <p>
     * <b>Time complexity:</b> O(1)
     * 
     * @param key
     * @param fields
     * @return If the field was present in the hash it is deleted and 1 is
     *         returned, otherwise 0 is returned and no operation is performed.
     */
    public Long hdel(String key, final String... fields) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.hdel(key, fields);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.hdel(key, fields);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Return the number of items in a hash.
     * <p>
     * <b>Time complexity:</b> O(1)
     * 
     * @param key
     * @return The number of entries (fields) contained in the hash stored at
     *         key. If the specified key does not exist, 0 is returned assuming
     *         an empty hash.
     */
    public Long hlen(String key) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.hlen(key);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.hlen(key);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Return all the fields in a hash.
     * <p>
     * <b>Time complexity:</b> O(N), where N is the total number of entries
     * 
     * @param key
     * @return All the fields names contained into a hash.
     */
    public Set<String> hkeys(String key) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.hkeys(key);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.hkeys(key);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Return all the values in a hash.
     * <p>
     * <b>Time complexity:</b> O(N), where N is the total number of entries
     * 
     * @param key
     * @return All the fields values contained into a hash.
     */
    public List<String> hvals(String key) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.hvals(key);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.hvals(key);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Return all the fields and associated values in a hash.
     * <p>
     * <b>Time complexity:</b> O(N), where N is the total number of entries
     * 
     * @param key
     * @return All the fields and values contained into a hash.
     */
    public Map<String, String> hgetAll(String key) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.hgetAll(key);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.hgetAll(key);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Add the string value to the head (LPUSH) or tail (RPUSH) of the list
     * stored at key. If the key does not exist an empty list is created just
     * before the append operation. If the key exists but is not a List an error
     * is returned.
     * <p>
     * Time complexity: O(1)
     * 
     * @param key
     * @param strings
     * @return Integer reply, specifically, the number of elements inside the
     *         list after the push operation.
     */
    public Long rpush(String key, final String... strings) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.rpush(key, strings);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.rpush(key, strings);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Add the string value to the head (LPUSH) or tail (RPUSH) of the list
     * stored at key. If the key does not exist an empty list is created just
     * before the append operation. If the key exists but is not a List an error
     * is returned.
     * <p>
     * Time complexity: O(1)
     * 
     * @param key
     * @param strings
     * @return Integer reply, specifically, the number of elements inside the
     *         list after the push operation.
     */
    public Long lpush(String key, final String... strings) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.lpush(key, strings);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.lpush(key, strings);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Return the length of the list stored at the specified key. If the key
     * does not exist zero is returned (the same behaviour as for empty lists).
     * If the value stored at key is not a list an error is returned.
     * <p>
     * Time complexity: O(1)
     * 
     * @param key
     * @return The length of the list.
     */
    public Long llen(String key) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.llen(key);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.llen(key);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Return the specified elements of the list stored at the specified key.
     * Start and end are zero-based indexes. 0 is the first element of the list
     * (the list head), 1 the next element and so on.
     * <p>
     * For example LRANGE foobar 0 2 will return the first three elements of the
     * list.
     * <p>
     * start and end can also be negative numbers indicating offsets from the
     * end of the list. For example -1 is the last element of the list, -2 the
     * penultimate element and so on.
     * <p>
     * <b>Consistency with range functions in various programming languages</b>
     * <p>
     * Note that if you have a list of numbers from 0 to 100, LRANGE 0 10 will
     * return 11 elements, that is, rightmost item is included. This may or may
     * not be consistent with behavior of range-related functions in your
     * programming language of choice (think Ruby's Range.new, Array#slice or
     * Python's range() function).
     * <p>
     * LRANGE behavior is consistent with one of Tcl.
     * <p>
     * <b>Out-of-range indexes</b>
     * <p>
     * Indexes out of range will not produce an error: if start is over the end
     * of the list, or start &gt; end, an empty list is returned. If end is over
     * the end of the list Redis will threat it just like the last element of
     * the list.
     * <p>
     * Time complexity: O(start+n) (with n being the length of the range and
     * start being the start offset)
     * 
     * @param key
     * @param start
     * @param end
     * @return Multi bulk reply, specifically a list of elements in the
     *         specified range.
     */
    public List<String> lrange(String key, final long start, final long end) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.lrange(key, start, end);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.lrange(key, start, end);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Trim an existing list so that it will contain only the specified range of
     * elements specified. Start and end are zero-based indexes. 0 is the first
     * element of the list (the list head), 1 the next element and so on.
     * <p>
     * For example LTRIM foobar 0 2 will modify the list stored at foobar key so
     * that only the first three elements of the list will remain.
     * <p>
     * start and end can also be negative numbers indicating offsets from the
     * end of the list. For example -1 is the last element of the list, -2 the
     * penultimate element and so on.
     * <p>
     * Indexes out of range will not produce an error: if start is over the end
     * of the list, or start &gt; end, an empty list is left as value. If end
     * over the end of the list Redis will threat it just like the last element
     * of the list.
     * <p>
     * Hint: the obvious use of LTRIM is together with LPUSH/RPUSH. For example:
     * <p>
     * {@code lpush("mylist", "someelement"); ltrim("mylist", 0, 99); * }
     * <p>
     * The above two commands will push elements in the list taking care that
     * the list will not grow without limits. This is very useful when using
     * Redis to store logs for example. It is important to note that when used
     * in this way LTRIM is an O(1) operation because in the average case just
     * one element is removed from the tail of the list.
     * <p>
     * Time complexity: O(n) (with n being len of list - len of range)
     * 
     * @param key
     * @param start
     * @param end
     * @return Status code reply
     */
    public String ltrim(String key, final long start, final long end) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.ltrim(key, start, end);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.ltrim(key, start, end);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Return the specified element of the list stored at the specified key. 0
     * is the first element, 1 the second and so on. Negative indexes are
     * supported, for example -1 is the last element, -2 the penultimate and so
     * on.
     * <p>
     * If the value stored at key is not of list type an error is returned. If
     * the index is out of range a 'nil' reply is returned.
     * <p>
     * Note that even if the average time complexity is O(n) asking for the
     * first or the last element of the list is O(1).
     * <p>
     * Time complexity: O(n) (with n being the length of the list)
     * 
     * @param key
     * @param index
     * @return Bulk reply, specifically the requested element
     */
    public String lindex(String key, final long index) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.lindex(key, index);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.lindex(key, index);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Set a new value as the element at index position of the List at key.
     * <p>
     * Out of range indexes will generate an error.
     * <p>
     * Similarly to other list commands accepting indexes, the index can be
     * negative to access elements starting from the end of the list. So -1 is
     * the last element, -2 is the penultimate, and so forth.
     * <p>
     * <b>Time complexity:</b>
     * <p>
     * O(N) (with N being the length of the list), setting the first or last
     * elements of the list is O(1).
     * 
     * @see #lindex(String, long)
     * @param key
     * @param index
     * @param value
     * @return Status code reply
     */
    public String lset(String key, final long index, final String value) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.lset(key, index, value);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.lset(key, index, value);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Remove the first count occurrences of the value element from the list. If
     * count is zero all the elements are removed. If count is negative elements
     * are removed from tail to head, instead to go from head to tail that is
     * the normal behaviour. So for example LREM with count -2 and hello as
     * value to remove against the list (a,b,c,hello,x,hello,hello) will lave
     * the list (a,b,c,hello,x). The number of removed elements is returned as
     * an integer, see below for more information about the returned value. Note
     * that non existing keys are considered like empty lists by LREM, so LREM
     * against non existing keys will always return 0.
     * <p>
     * Time complexity: O(N) (with N being the length of the list)
     * 
     * @param key
     * @param count
     * @param value
     * @return Integer Reply, specifically: The number of removed elements if
     *         the operation succeeded
     */
    public Long lrem(String key, final long count, final String value) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.lrem(key, count, value);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.lrem(key, count, value);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Atomically return and remove the first (LPOP) or last (RPOP) element of
     * the list. For example if the list contains the elements "a","b","c" LPOP
     * will return "a" and the list will become "b","c".
     * <p>
     * If the key does not exist or the list is already empty the special value
     * 'nil' is returned.
     * 
     * @see #rpop(String)
     * @param key
     * @return Bulk reply
     */
    public String lpop(String key) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.lpop(key);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.lpop(key);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Atomically return and remove the first (LPOP) or last (RPOP) element of
     * the list. For example if the list contains the elements "a","b","c" RPOP
     * will return "c" and the list will become "a","b".
     * <p>
     * If the key does not exist or the list is already empty the special value
     * 'nil' is returned.
     * 
     * @see #lpop(String)
     * @param key
     * @return Bulk reply
     */
    public String rpop(String key) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.rpop(key);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.rpop(key);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Atomically return and remove the last (tail) element of the srckey list,
     * and push the element as the first (head) element of the dstkey list. For
     * example if the source list contains the elements "a","b","c" and the
     * destination list contains the elements "foo","bar" after an RPOPLPUSH
     * command the content of the two lists will be "a","b" and "c","foo","bar".
     * <p>
     * If the key does not exist or the list is already empty the special value
     * 'nil' is returned. If the srckey and dstkey are the same the operation is
     * equivalent to removing the last element from the list and pusing it as
     * first element of the list, so it's a "list rotation" command.
     * <p>
     * Time complexity: O(1)
     * 
     * @param srckey
     * @param dstkey
     * @return Bulk reply
     */
    public String rpoplpush(String srckey, String dstkey) {
        srckey = namespace + srckey;
        dstkey = namespace + dstkey;
        if (isCluster) {
            return jedisCluster.rpoplpush(srckey, dstkey);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.rpoplpush(srckey, dstkey);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Add the specified member to the set value stored at key. If member is
     * already a member of the set no operation is performed. If key does not
     * exist a new set with the specified member as sole member is created. If
     * the key exists but does not hold a set value an error is returned.
     * <p>
     * Time complexity O(1)
     * 
     * @param key
     * @param members
     * @return Integer reply, specifically: 1 if the new element was added 0 if
     *         the element was already a member of the set
     */
    public Long sadd(String key, final String... members) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.sadd(key, members);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.sadd(key, members);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Return all the members (elements) of the set value stored at key. This is
     * just syntax glue for {@link #sinter(String...) SINTER}.
     * <p>
     * Time complexity O(N)
     * 
     * @param key
     * @return Multi bulk reply
     */
    public Set<String> smembers(String key) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.smembers(key);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.smembers(key);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Remove the specified member from the set value stored at key. If member
     * was not a member of the set no operation is performed. If key does not
     * hold a set value an error is returned.
     * <p>
     * Time complexity O(1)
     * 
     * @param key
     * @param members
     * @return Integer reply, specifically: 1 if the new element was removed 0
     *         if the new element was not a member of the set
     */
    public Long srem(String key, final String... members) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.srem(key, members);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.srem(key, members);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Remove a random element from a Set returning it as return value. If the
     * Set is empty or the key does not exist, a nil object is returned.
     * <p>
     * The {@link #srandmember(String)} command does a similar work but the
     * returned element is not removed from the Set.
     * <p>
     * Time complexity O(1)
     * 
     * @param key
     * @return Bulk reply
     */
    public String spop(String key) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.spop(key);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.spop(key);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    public Set<String> spop(String key, final long count) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.spop(key, count);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.spop(key, count);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Move the specifided member from the set at srckey to the set at dstkey.
     * This operation is atomic, in every given moment the element will appear
     * to be in the source or destination set for accessing clients.
     * <p>
     * If the source set does not exist or does not contain the specified
     * element no operation is performed and zero is returned, otherwise the
     * element is removed from the source set and added to the destination set.
     * On success one is returned, even if the element was already present in
     * the destination set.
     * <p>
     * An error is raised if the source or destination keys contain a non Set
     * value.
     * <p>
     * Time complexity O(1)
     * 
     * @param srckey
     * @param dstkey
     * @param member
     * @return Integer reply, specifically: 1 if the element was moved 0 if the
     *         element was not found on the first set and no operation was
     *         performed
     */
    public Long smove(String srckey, String dstkey, final String member) {
        srckey = namespace + srckey;
        dstkey = namespace + dstkey;
        if (isCluster) {
            return jedisCluster.smove(srckey, dstkey, member);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.smove(srckey, dstkey, member);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Return the set cardinality (number of elements). If the key does not
     * exist 0 is returned, like for empty sets.
     * 
     * @param key
     * @return Integer reply, specifically: the cardinality (number of elements)
     *         of the set as an integer.
     */
    public Long scard(String key) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.scard(key);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.scard(key);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Return 1 if member is a member of the set stored at key, otherwise 0 is
     * returned.
     * <p>
     * Time complexity O(1)
     * 
     * @param key
     * @param member
     * @return Integer reply, specifically: 1 if the element is a member of the
     *         set 0 if the element is not a member of the set OR if the key
     *         does not exist
     */
    public Boolean sismember(String key, final String member) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.sismember(key, member);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.sismember(key, member);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Return the members of a set resulting from the intersection of all the
     * sets hold at the specified keys. Like in
     * {@link #lrange(String, long, long) LRANGE} the result is sent to the
     * client as a multi-bulk reply (see the protocol specification for more
     * information). If just a single key is specified, then this command
     * produces the same result as {@link #smembers(String) SMEMBERS}. Actually
     * SMEMBERS is just syntax sugar for SINTER.
     * <p>
     * Non existing keys are considered like empty sets, so if one of the keys
     * is missing an empty set is returned (since the intersection with an empty
     * set always is an empty set).
     * <p>
     * Time complexity O(N*M) worst case where N is the cardinality of the
     * smallest set and M the number of sets
     * 
     * @param keys
     * @return Multi bulk reply, specifically the list of common elements.
     */
    public Set<String> sinter(String... keys) {
        String[] newKeys = transformKeys(keys);
        if (isCluster) {
            return jedisCluster.sinter(newKeys);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.sinter(newKeys);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * This commnad works exactly like {@link #sinter(String...) SINTER} but
     * instead of being returned the resulting set is sotred as dstkey.
     * <p>
     * Time complexity O(N*M) worst case where N is the cardinality of the
     * smallest set and M the number of sets
     * 
     * @param dstkey
     * @param keys
     * @return Status code reply
     */
    public Long sinterstore(String dstkey, String... keys) {
        dstkey = namespace + dstkey;
        String[] newKeys = transformKeys(keys);
        if (isCluster) {
            return jedisCluster.sinterstore(dstkey, newKeys);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.sinterstore(dstkey, newKeys);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Return the members of a set resulting from the union of all the sets hold
     * at the specified keys. Like in {@link #lrange(String, long, long) LRANGE}
     * the result is sent to the client as a multi-bulk reply (see the protocol
     * specification for more information). If just a single key is specified,
     * then this command produces the same result as {@link #smembers(String)
     * SMEMBERS}.
     * <p>
     * Non existing keys are considered like empty sets.
     * <p>
     * Time complexity O(N) where N is the total number of elements in all the
     * provided sets
     * 
     * @param keys
     * @return Multi bulk reply, specifically the list of common elements.
     */
    public Set<String> sunion(String... keys) {
        String[] newKeys = transformKeys(keys);
        if (isCluster) {
            return jedisCluster.sunion(newKeys);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.sunion(newKeys);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * This command works exactly like {@link #sunion(String...) SUNION} but
     * instead of being returned the resulting set is stored as dstkey. Any
     * existing value in dstkey will be over-written.
     * <p>
     * Time complexity O(N) where N is the total number of elements in all the
     * provided sets
     * 
     * @param dstkey
     * @param keys
     * @return Status code reply
     */
    public Long sunionstore(String dstkey, String... keys) {
        dstkey = namespace + dstkey;
        String[] newKeys = transformKeys(keys);
        if (isCluster) {
            return jedisCluster.sunionstore(dstkey, newKeys);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.sunionstore(dstkey, newKeys);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Return the difference between the Set stored at key1 and all the Sets
     * key2, ..., keyN
     * <p>
     * <b>Example:</b>
     * 
     * <pre>
     * key1 = [x, a, b, c]
     * key2 = [c]
     * key3 = [a, d]
     * SDIFF key1,key2,key3 =&gt; [x, b]
     * </pre>
     * 
     * Non existing keys are considered like empty sets.
     * <p>
     * <b>Time complexity:</b>
     * <p>
     * O(N) with N being the total number of elements of all the sets
     * 
     * @param keys
     * @return Return the members of a set resulting from the difference between
     *         the first set provided and all the successive sets.
     */
    public Set<String> sdiff(final String... keys) {
        String[] newKeys = transformKeys(keys);
        if (isCluster) {
            return jedisCluster.sdiff(newKeys);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.sdiff(newKeys);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * This command works exactly like {@link #sdiff(String...) SDIFF} but
     * instead of being returned the resulting set is stored in dstkey.
     * 
     * @param dstkey
     * @param keys
     * @return Status code reply
     */
    public Long sdiffstore(String dstkey, final String... keys) {
        dstkey = namespace + dstkey;
        String[] newKeys = transformKeys(keys);
        if (isCluster) {
            return jedisCluster.sdiffstore(dstkey, newKeys);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.sdiffstore(dstkey, newKeys);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Return a random element from a Set, without removing the element. If the
     * Set is empty or the key does not exist, a nil object is returned.
     * <p>
     * The SPOP command does a similar work but the returned element is popped
     * (removed) from the Set.
     * <p>
     * Time complexity O(1)
     * 
     * @param key
     * @return Bulk reply
     */
    public String srandmember(String key) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.srandmember(key);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.srandmember(key);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    public List<String> srandmember(String key, final int count) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.srandmember(key, count);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.srandmember(key, count);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Add the specified member having the specifeid score to the sorted set
     * stored at key. If member is already a member of the sorted set the score
     * is updated, and the element reinserted in the right position to ensure
     * sorting. If key does not exist a new sorted set with the specified member
     * as sole member is crated. If the key exists but does not hold a sorted
     * set value an error is returned.
     * <p>
     * The score value can be the string representation of a double precision
     * floating point number.
     * <p>
     * Time complexity O(log(N)) with N being the number of elements in the
     * sorted set
     * 
     * @param key
     * @param score
     * @param member
     * @return Integer reply, specifically: 1 if the new element was added 0 if
     *         the element was already a member of the sorted set and the score
     *         was updated
     */
    public Long zadd(String key, final double score, final String member) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.zadd(key, score, member);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.zadd(key, score, member);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    public Long zadd(String key, final double score, final String member, final ZAddParams params) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.zadd(key, score, member, params);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.zadd(key, score, member, params);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    public Long zadd(String key, final Map<String, Double> scoreMembers) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.zadd(key, scoreMembers);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.zadd(key, scoreMembers);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    public Long zadd(String key, Map<String, Double> scoreMembers, ZAddParams params) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.zadd(key, scoreMembers, params);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.zadd(key, scoreMembers, params);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    public Set<String> zrange(String key, final long start, final long end) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.zrange(key, start, end);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.zrange(key, start, end);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Remove the specified member from the sorted set value stored at key. If
     * member was not a member of the set no operation is performed. If key does
     * not not hold a set value an error is returned.
     * <p>
     * Time complexity O(log(N)) with N being the number of elements in the
     * sorted set
     * 
     * @param key
     * @param members
     * @return Integer reply, specifically: 1 if the new element was removed 0
     *         if the new element was not a member of the set
     */
    public Long zrem(String key, final String... members) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.zrem(key, members);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.zrem(key, members);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * If member already exists in the sorted set adds the increment to its
     * score and updates the position of the element in the sorted set
     * accordingly. If member does not already exist in the sorted set it is
     * added with increment as score (that is, like if the previous score was
     * virtually zero). If key does not exist a new sorted set with the
     * specified member as sole member is crated. If the key exists but does not
     * hold a sorted set value an error is returned.
     * <p>
     * The score value can be the string representation of a double precision
     * floating point number. It's possible to provide a negative value to
     * perform a decrement.
     * <p>
     * For an introduction to sorted sets check the Introduction to Redis data
     * types page.
     * <p>
     * Time complexity O(log(N)) with N being the number of elements in the
     * sorted set
     * 
     * @param key
     * @param score
     * @param member
     * @return The new score
     */
    public Double zincrby(String key, final double score, final String member) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.zincrby(key, score, member);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.zincrby(key, score, member);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    public Double zincrby(String key, double score, String member, ZIncrByParams params) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.zincrby(key, score, member, params);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.zincrby(key, score, member, params);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Return the rank (or index) or member in the sorted set at key, with
     * scores being ordered from low to high.
     * <p>
     * When the given member does not exist in the sorted set, the special value
     * 'nil' is returned. The returned rank (or index) of the member is 0-based
     * for both commands.
     * <p>
     * <b>Time complexity:</b>
     * <p>
     * O(log(N))
     * 
     * @see #zrevrank(String, String)
     * @param key
     * @param member
     * @return Integer reply or a nil bulk reply, specifically: the rank of the
     *         element as an integer reply if the element exists. A nil bulk
     *         reply if there is no such element.
     */
    public Long zrank(String key, final String member) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.zrank(key, member);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.zrank(key, member);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Return the rank (or index) or member in the sorted set at key, with
     * scores being ordered from high to low.
     * <p>
     * When the given member does not exist in the sorted set, the special value
     * 'nil' is returned. The returned rank (or index) of the member is 0-based
     * for both commands.
     * <p>
     * <b>Time complexity:</b>
     * <p>
     * O(log(N))
     * 
     * @see #zrank(String, String)
     * @param key
     * @param member
     * @return Integer reply or a nil bulk reply, specifically: the rank of the
     *         element as an integer reply if the element exists. A nil bulk
     *         reply if there is no such element.
     */
    public Long zrevrank(String key, final String member) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.zrevrank(key, member);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.zrevrank(key, member);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    public Set<String> zrevrange(String key, final long start, final long end) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.zrevrange(key, start, end);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.zrevrange(key, start, end);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    public Set<Tuple> zrangeWithScores(String key, final long start, final long end) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.zrangeWithScores(key, start, end);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.zrangeWithScores(key, start, end);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    public Set<Tuple> zrevrangeWithScores(String key, final long start, final long end) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.zrevrangeWithScores(key, start, end);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.zrevrangeWithScores(key, start, end);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Return the sorted set cardinality (number of elements). If the key does
     * not exist 0 is returned, like for empty sorted sets.
     * <p>
     * Time complexity O(1)
     * 
     * @param key
     * @return the cardinality (number of elements) of the set as an integer.
     */
    public Long zcard(String key) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.zcard(key);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.zcard(key);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    /**
     * Return the score of the specified element of the sorted set at key. If
     * the specified element does not exist in the sorted set, or the key does
     * not exist at all, a special 'nil' value is returned.
     * <p>
     * <b>Time complexity:</b> O(1)
     * 
     * @param key
     * @param member
     * @return the score
     */
    public Double zscore(String key, final String member) {
        key = namespace + key;
        if (isCluster) {
            return jedisCluster.zscore(key, member);
        } else {
            Jedis jedis = getResource();
            try{
                return jedis.zscore(key, member);
            }finally {
                returnResource(jedis); 
            }
        }
    }

    private String[] transformKeys(String... keys) {
        String[] rs = new String[keys.length];
        for (int i = 0; i < rs.length; i++) {
            rs[i] = namespace + keys[i];
        }
        return rs;
    }
}