package com.ihome.framework.core.cache.redis;

import org.springframework.data.redis.core.RedisTemplate;

import com.ihome.framework.core.cache.IHomeCacheClient;
import com.ihome.framework.core.cache.nkv.NkvResult;
import com.ihome.framework.core.exception.CoreErrors;
import com.netease.backend.nkv.client.Result;
import com.netease.backend.nkv.client.NkvClient.NkvOption;

public interface IRedisClient extends IHomeCacheClient {
	

	void deleteIfNotExists(String key);
	
	
	/**
	 * 对某个key的值做加法
	 * @param key
	 *            操作的key
	 * @param value
	 *            要加上多少
	 * @param initValue
	 *            若key不存在的时候，新建key并指定为此初始值，然后再做加法
	 * @return 做完减法操作后的值
	 */

	int incr(String key, int value, int initValue);
	
	
}
