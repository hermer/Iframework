package com.ihome.framework.core.cache.redis;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.util.ReflectionUtils;

import com.ihome.framework.core.cache.IHomeCacheClient;
import com.ihome.framework.core.exception.PlatformException;

public class RedisAsyncClient implements IRedisClient {
	private static final Logger LOG = LoggerFactory.getLogger(RedisAsyncClient.class);
	
	private RedisTemplate<String,Object> redisTemplate;
	
	
	private ThreadPoolTaskExecutor redisAsyncExecutor;
	
	
	
	
	public RedisAsyncClient() {
		super();
	}



	public RedisAsyncClient(RedisTemplate<String, Object> redisTemplate, ThreadPoolTaskExecutor redisAsyncExecutor) {
		super();
		this.redisTemplate = redisTemplate;
		this.redisAsyncExecutor = redisAsyncExecutor;
	}



	/**
	 * 放入缓存
	 * 
	 * @param key
	 *            key
	 * @param value
	 *            值
	 */
	public void set(String key, Object value) {
		redisAsyncExecutor.execute(new RedisAsyncRunnable(redisTemplate.opsForValue(), "set", new Class<?>[] {Object.class,Object.class},new Object[] {key,value}));
	}

	
	
	/**
	 * 放入缓存
	 * 
	 * @param key
	 *            key
	 * @param exp
	 *            过去时间，单位秒
	 * @param value
	 *            值
	 */
	public void set(String key, Object value, int exp) {
		redisAsyncExecutor.execute(new RedisAsyncRunnable(redisTemplate.opsForValue(), "set", new Class<?>[] {Object.class,Object.class,long.class,TimeUnit.class},new Object[] {key,value,exp,TimeUnit.SECONDS}));
	}

	/**
	 * 当对应的key不存在时 放入缓存
	 * 如果key存在 参考NKV接口实现 如果key值存在抛 运行时异常
	 * 
	 * @param key
	 *            key
	 * @param value
	 *            值
	 */
	public void setIfNotExist(final String key, final Object value) {
		redisAsyncExecutor.execute(new Runnable() {
			public void run() {
				try {
					redisAsyncExecutor.execute(new RedisAsyncRunnable(redisTemplate.opsForValue(), "setIfAbsent", new Class<?>[] {Object.class,Object.class},new Object[] {key,value}));
				} catch (Exception e) {
					LOG.error("async operate redis {} for params{} exception {}","setIfNotExist",new Object[] {key,value},e);
				}
			}
		});
	}

	/**
	 * 当对应的key不存在时 放入缓存
	 * 
	 * @param key
	 *            key
	 * @param value
	 *            值
	 * @param exp
	 *            过期时间，单位秒
	 */
	public void setIfNotExist(final String key, final Object value, final int exp) {
		redisAsyncExecutor.execute(new Runnable() {
			public void run() {
				try {
					boolean result=redisTemplate.opsForValue().setIfAbsent(key, value);
					if(!result) {
						throw new PlatformException("async redis的setIfNotExist key value 操作异常,"+key+"is already exists");
					}
					redisTemplate.expire(key, exp, TimeUnit.SECONDS);
				} catch (Exception e) {
					LOG.error("async operate redis {} for params{} exception {}","setIfNotExist",new Object[] {key,value},e);
				}
			}
		});
	}

	/**
	 * 从缓存中获取对象
	 * 
	 * @param key
	 *            key
	 * @return Object
	 */
	public Object get(String key) {
		return redisTemplate.opsForValue().get(key);
	}

	/**
	 * 根据key获取对象
	 * 
	 * @param key
	 *            key
	 * @param t
	 *            类型
	 * @param <T>
	 *            类型
	 * @return object
	 */
	public <T> T get(String key, Class<T> t) {
		return (T)redisTemplate.opsForValue().get(key);
	}

	/**
	 * 从缓存中删除
	 * 
	 * @param key
	 *            cache key
	 */
	public void delete(String key) {
		redisAsyncExecutor.execute(new RedisAsyncRunnable(redisTemplate.opsForValue(), "delete", new Class<?>[] {Object.class},new Object[] {key}));
	}
	
	@Override
	public void deleteIfNotExists(final String key) {
		redisAsyncExecutor.execute(new Runnable() {
			public void run() {
				try {
					final byte[] rawKey = redisTemplate.getStringSerializer().serialize(key);
					Long result=redisTemplate.execute(new RedisCallback<Long>() {
						public Long doInRedis(RedisConnection connection) {
							Long n=connection.del(rawKey);
							return n;
						}
					}, true);
					if(result<=0)
						throw new PlatformException("redis的deleteIfNotExists key 操作异常,不存在key值["+key+"]");
				} catch (Exception e) {
					LOG.error("async operate redis {} for params{} exception {}","setIfNotExist",new Object[] {key},e);
				}
			}
		});
		
	}

	


	@Override
	public int incr(String key, int value, int initValue) {
		int result=-1;
        if(!redisTemplate.hasKey(key)) {
        	redisTemplate.opsForValue().set(key, initValue);
        }
        result=Integer.valueOf(redisTemplate.opsForValue().increment(key, value).toString());
        return result;
	}
	
	
	
	
	
	
	/**
	 * 简单调用异步方法
	 * @author xiaoweijun 
	 *
	 */
	class RedisAsyncRunnable implements Runnable {
		private Logger LOG = LoggerFactory.getLogger(RedisAsyncRunnable.class);
		private String methodName;
	    private Object[] args;
	    private Class<?>[] paramTypes;
	    private Object target;

	    public RedisAsyncRunnable(Object target,String methodName,  Class<?>[] paramTypes, Object[] args) {
			super();
			this.methodName = methodName;
			this.args = args;
			this.paramTypes = paramTypes;
			this.target = target;
		}

		@Override
	    public void run() {
			try {
			   Method method= ReflectionUtils.findMethod(target.getClass(),methodName,paramTypes);
		       method.setAccessible(true);
		       ReflectionUtils.invokeMethod(method,target,args);
			} catch (Exception e) {
				LOG.error("async operate redis {} for params{} exception {}",methodName,args,e);
			}
	    }
	}
	
	
	
	
	
	/***********************************get and set method*******************************************************/
	
	public RedisTemplate<String, Object> getRedisTemplate() {
		return redisTemplate;
	}



	public ThreadPoolTaskExecutor getRedisAsyncExecutor() {
		return redisAsyncExecutor;
	}



	public void setRedisTemplate(RedisTemplate<String, Object> redisTemplate) {
		this.redisTemplate = redisTemplate;
	}



	public void setRedisAsyncExecutor(ThreadPoolTaskExecutor redisAsyncExecutor) {
		this.redisAsyncExecutor = redisAsyncExecutor;
	}



	
	

}
