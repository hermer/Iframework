/**
 * 
 */
package com.ihome.framework.core.cache;

import java.util.concurrent.ThreadPoolExecutor;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.ihome.framework.core.cache.nkv.IHomeNkvClient;
import com.ihome.framework.core.cache.nkv.NkvResult;
import com.ihome.framework.core.cache.redis.IRedisClient;
import com.ihome.framework.core.cache.redis.RedisAsyncClient;
import com.ihome.framework.core.cache.redis.RedisSyncClient;
import com.ihome.framework.core.exception.ApplicationException;
import com.ihome.framework.core.exception.CoreErrors;
import com.netease.backend.nkv.client.Result;
import com.netease.backend.nkv.client.NkvClient.NkvOption;
import com.netease.backend.nkv.client.Result.ResultCode;

/**
 * 缓存客户端的实现，包装NKV 和 redis操作
 * 根据配置来读写缓存
 * 
 * 为了兼容NKV的客户端 重写所有的的NKV的公共方法
 * 迁移环境对缓存的操作有三种情况
 * 1,不使用redis，延用旧的API读写NKV
 * 2,双写期间：同步读写NKV，异步写redis
 * 3,切换：使用新的API同步读写redis,不操作NKV
 * 
 * @author xiaoweijun
 */
public class IHomeCacheClientImpl extends IHomeNkvClient implements  IHomeCacheClient{
	private IHomeNkvClient nkvClient;
	private IRedisClient redisClient;

	public IHomeCacheClientImpl() {
		super();
		
	}

	@SuppressWarnings("unchecked")
	public IHomeCacheClientImpl(IHomeNkvClient nkvClient, RedisTemplate redisTemplate) {
		super();
		this.nkvClient = nkvClient;
		if(nkvClient != null && redisTemplate !=null) {
			ThreadPoolTaskExecutor redisAsyncExecutor=new ThreadPoolTaskExecutor();
			redisAsyncExecutor.setCorePoolSize(5);
			redisAsyncExecutor.setKeepAliveSeconds(180);
			redisAsyncExecutor.setMaxPoolSize(20);
			redisAsyncExecutor.setQueueCapacity(20000);
			redisAsyncExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
			redisAsyncExecutor.initialize();
			redisClient=new RedisAsyncClient(redisTemplate, redisAsyncExecutor);
		}
		else if (redisTemplate !=null) {
			redisClient=new RedisSyncClient(redisTemplate);
		}
	}


	
	
	
	

	/**
	 * -------------------incrVersion----------------------------
	 */
	/**
	 * 递增一下版本号 
	 * 不用修改，不能直接调用nkvClient 可能为null
	 * 
	 * @param version
	 *            版本号
	 * @return
	 */
	public short incrVersion(short version) {
		if (version < Short.MAX_VALUE) {
			return (short) (version + 1);
		} else {
			return 2;
		}
	}



	/**
	 * -------------------set----------------------------
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ihome.framework.core.cache.IHomeCacheClient#set(java.lang.String,
	 * java.lang.Object)
	 */
	@Override
	public void set(String key, Object value) {
		if(null != nkvClient) {
			nkvClient.set(key, value);
		}
		if(null != redisClient)
			redisClient.set(key, value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ihome.framework.core.cache.IHomeCacheClient#set(java.lang.String,
	 * java.lang.Object, int)
	 */
	@Override
	public void set(String key, Object value, int exp) {
		if(null != nkvClient) {
			nkvClient.set(key, value,exp);
		}
		if(null != redisClient)
			redisClient.set(key, value,exp);
	}

	/**
	 * 放入缓存，指定key的前缀
	 * 
	 * @param prefix
	 *            前缀
	 * @param key
	 *            操作的key
	 * @param value
	 *            key的值
	 */
	public void set(String prefix, String key, Object value) {
		String finalKey = prefix + key;
		set(finalKey,value);
	}

	/**
	 * 放入缓存，指定key的前缀和nkv相关配置
	 * 
	 * @param prefix
	 *            前缀
	 * @param key
	 *            操作的key
	 * @param value
	 *            key的值
	 * @param opt
	 *            opt中的expire单位是秒
	 */
	public void set(String prefix, String key, Object value, NkvOption opt) {
		if(null != nkvClient) {
			nkvClient.set(prefix,key, value,opt);
		}
		if(null != redisClient)
			redisClient.set(prefix+key, value);
	}

	/**
	 * 放入缓存，指定key的前缀和过期时间
	 * 
	 * @param prefix
	 *            前缀
	 * @param key
	 *            操作的key
	 * @param value
	 *            key的值
	 * @param exp
	 *            过期时间，单位是秒
	 */
	public void set(String prefix, String key, Object value, int exp) {
		if(null != nkvClient) {
			nkvClient.set(prefix,key, value,exp);
		}
		if(null != redisClient)
			redisClient.set(prefix+key, value,exp);
	}

	/**
	 * 放入缓存，指定key的前缀和过期时间和nkv相关配置
	 * 
	 * @param prefix
	 *            前缀
	 * @param key
	 *            操作的key
	 * @param value
	 *            key的值
	 * @param exp
	 *            过期时间，单位是秒
	 * @param opt
	 *            nkv操作的选项
	 */
	public void set(String prefix, String key, Object value, int exp, NkvOption opt) {
		if(null != nkvClient) {
			nkvClient.set(prefix,key, value,exp,opt);
		}
		if(null != redisClient)
			redisClient.set(prefix+key, value,exp);
	}

	
	/**
	 * 放入缓存，并返回nkv返回的结果
	 * 
	 * @param prefix
	 *            前缀
	 * @param key
	 *            操作的key
	 * @param value
	 *            key的值
	 * @param opt
	 *            opt中的expire单位是秒
	 * @return
	 */
	public NkvResult<Void> setResult(String prefix, String key, Object value, NkvOption opt) {
		if(null != nkvClient) {
			return nkvClient.setResult(prefix,key, value,opt);
		}
		if(null != redisClient) {
			redisClient.set(prefix+key, value);
			return getVoidResult(prefix+key);
		}
		return null;
	}
	
	/**
	 * 返回空对象 兼容NKV接口 有待验证
	 * @param key
	 * @return
	 */
	private NkvResult<Void> getVoidResult(String key){
		Result<Void> originResult =new Result<>(ResultCode.OK);
		originResult.setKey(key.getBytes());
		NkvResult<Void> result=new NkvResult<Void>(originResult);
		return result;
	}
	

	/**
	 * -------------------get----------------------------
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ihome.framework.core.cache.IHomeCacheClient#get(java.lang.String)
	 */
	@Override
	public Object get(String key) {
		if(null != nkvClient) {
			return nkvClient.get(key);
		}
		if(null != redisClient)
			return redisClient.get(key);
		return null;
	}

	/**
	 * 从缓存中获取对象，指定key的前缀
	 * 
	 * @param prefix
	 *            前缀
	 * @param key
	 *            操作的key
	 * @return
	 */
	public Object get(String prefix, String key) {
		if(null != nkvClient) {
			return nkvClient.get(prefix,key);
		}
		if(null != redisClient)
			return redisClient.get(prefix+key);
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ihome.framework.core.cache.I`HomeCacheClient#get(java.lang.String,
	 * java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> T get(String key, Class<T> t) {
		if(null != nkvClient) {
			return nkvClient.get(key,t);
		}
		if(null != redisClient)
			return (T)redisClient.get(key);
		return null;
	}

	/**
	 * 从缓存中获取对象，指定key的前缀和value的具体类型
	 * 
	 * @param prefix
	 *            前缀
	 * @param key
	 *            操作的key
	 * @param <T>
	 * @param t
	 *            value的类型
	 * @return
	 */
	public <T> T get(String prefix, String key, Class<T> t) {
		return (T) get(prefix+key,t);
	}
	
	
	/**
	 * 从缓存中获取对象，返回nkv返回的result
	 * 
	 * @param prefix
	 *            前缀
	 * @param key
	 *            操作的key
	 * @return
	 */
	public NkvResult<Object> getResult(String prefix, String key) {
		if(null != nkvClient) {
			return nkvClient.getResult(prefix,key);
		}
		if(null != redisClient) {
			Object value=redisClient.get(prefix+key);
			return getObjectResult(prefix+key,value);
		}	
		return null;
	}
	
	
	/**
	 * 返回Object对象 兼容NKV接口 有待验证
	 * @param key
	 * @return
	 */
	private NkvResult<Object> getObjectResult(String key,Object value){
		Result<Object> originResult =new Result<>(ResultCode.OK);
		originResult.setKey(key.getBytes());
		originResult.setResult(value);
		NkvResult<Object> result=new NkvResult<Object>(originResult);
		return result;
	}

	

	/**
	 * -------------------delete----------------------------
	 */
	/*
	 * 
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ihome.framework.core.cache.IHomeCacheClient#delete(java.lang.String)
	 */
	@Override
	public void delete(String key) {
		if(null != nkvClient) {
			nkvClient.delete(key);
		}
		if(null != redisClient)
			redisClient.delete(key);
	}

	/**
	 * 删除key
	 * 
	 * @param prefix
	 *            前缀
	 * @param key
	 *            操作的key
	 */
	public void delete(String prefix, String key) {
		delete(prefix+key);
	}

	/**
	 * 删除key，当key不存在的时候，抛出异常
	 * 
	 * @param key
	 *            操作的key
	 * @return
	 * @throws ApplicationException
	 */
	public void deleteIfNotExists(String key) {
		if(null != nkvClient) {
			nkvClient.deleteIfNotExists(key);
		}
		if(null != redisClient)
			redisClient.deleteIfNotExists(key);
	}

	/**
	 * 删除key，当key不存在的时候，抛出异常
	 * 
	 * @param prefix
	 *            前缀
	 * @param key
	 *            操作的key
	 */
	public void deleteIfNotExists(String prefix, String key) {
		String finalKey = prefix + key;
		deleteIfNotExists(finalKey);
	}

	/**
	 * -------------------setCount----------------------------
	 */
	/**
	 * 类似set接口，不过value为int类型，用于计数器之类的场景
	 * 
	 * @param prefix
	 *            前缀
	 * @param key
	 *            操作的key
	 * @param count
	 *            计数器的值
	 * @return
	 */
	public NkvResult<Void> setCount(String prefix, String key, int count) {
		if(null != nkvClient) {
			return nkvClient.setCount(prefix,key, count);
		}
		if(null != redisClient) {
			redisClient.set(prefix+key, count);
			return getVoidResult(prefix+key);
		}
		return null;
	}

	/**
	 * -------------------getCount----------------------------
	 */
	/**
	 * 对应setCount接口，获取计算器的值
	 * 
	 * @param prefix
	 *            前缀
	 * @param key
	 *            操作的key
	 * @return
	 */
	public int getCount(String prefix, String key) {
		if(null != nkvClient) {
			return nkvClient.getCount(prefix,key);
		}
		if(null != redisClient) {
			Object value=redisClient.get(prefix+key);
			return Integer.valueOf(value.toString());
		}	
		return -1;
	}

	/**
	 * -------------------decr----------------------------
	 */

	/**
	 * 对某个key的值做减法
	 * 
	 * @param prefix
	 *            前缀
	 * @param key
	 *            操作的key
	 * @param value
	 *            要减去多少
	 * @param initValue
	 *            若key不存在的时候，初始的值
	 * @return 做完减法操作后的值
	 */
	public int decr(String prefix, String key, int value, int initValue) {
		if(null != nkvClient) {
			return nkvClient.decr(prefix,key,value,initValue);
		}
		if(null != redisClient) {
			return  value=redisClient.incr(prefix+key,0-value,initValue);
		}	
		return -1;
	}

	/**
	 * 对某个key的值做减法
	 * 
	 * @param prefix
	 *            前缀
	 * @param key
	 *            操作的key
	 * @param value
	 *            要减去多少
	 * @param initValue
	 *            若key不存在的时候，新建key并指定为此初始值，然后再做减法
	 * @param opt
	 *            对decr方法，opt中只有timeout和expire是有用的
	 * @return 做完减法操作后的值
	 */
	public int decr(String prefix, String key, int value, int initValue, NkvOption opt) {
		if(null != nkvClient) {
			return nkvClient.decr(prefix,key,value,initValue,opt);
		}
		if(null != redisClient) {
			return  value=redisClient.incr(prefix+key,0-value,initValue);
		}	
		return -1;
	}

	/**
	 * -------------------incr----------------------------
	 */

	/**
	 * 对某个key的值做加法
	 * 
	 * @param prefix
	 *            前缀
	 * @param key
	 *            操作的key
	 * @param value
	 *            要加上多少
	 * @param initValue
	 *            若key不存在的时候，新建key并指定为此初始值，然后再做加法
	 * @return 做完减法操作后的值
	 */
	public int incr(String prefix, String key, int value, int initValue) {
		if(null != nkvClient) {
			return nkvClient.incr(prefix,key,value,initValue);
		}
		if(null != redisClient) {
			return  value=redisClient.incr(prefix+key,value,initValue);
		}	
		return -1;
	}

	/**
	 * 对某个key的值做加法
	 * 
	 * @param prefix
	 *            前缀
	 * @param key
	 *            操作的key
	 * @param value
	 *            要加上多少
	 * @param initValue
	 *            若key不存在的时候，新建key并指定为此初始值，然后再做加法
	 * @param opt
	 *            对incr方法，opt中只有timeout和expire是有用的
	 * @return 做完减法操作后的值
	 */
	public int incr(String prefix, String key, int value, int initValue, NkvOption opt) {
		if(null != nkvClient) {
			return nkvClient.incr(prefix,key,value,initValue,opt);
		}
		if(null != redisClient) {
			return  value=redisClient.incr(prefix+key,value,initValue);
		}	
		return -1;
	}

	/**
	 * -------------------setIfNotExist----------------------------
	 */

	
	 /* (non-Javadoc)
	 * 
	 * @see
	 * com.ihome.framework.core.cache.IHomeCacheClient#setIfNotExist(java.lang
	 * .String, java.lang.Object)
	 * */
	 
	@Override
	public void setIfNotExist(String key, Object value) {
		
		if(null != nkvClient) {
			nkvClient.setIfNotExist(key, value);
		}
		if(null != redisClient)
			redisClient.setIfNotExist(key, value);
	}

	/**
	 * 放入缓存，当该key已存在或网络出错，统一都抛出PlatformException
	 * 
	 * @param prefix
	 *            指定key的前缀
	 * @param key
	 *            放入nkv中的key
	 * @param value
	 *            值
	 */
	public void setIfNotExist(String prefix, String key, Object value) {
		String finalKey = prefix + key;
		setIfNotExist(finalKey, value);
	}

	
	/* * (non-Javadoc)
	 * 
	 * @see
	 * com.ihome.framework.core.cache.IHomeCacheClient#setIfNotExist(java.lang
	 * .String, java.lang.Object, int)*/
	 
	@Override
	public void setIfNotExist(String key, Object value, int exp) {
		if(null != nkvClient) {
			nkvClient.setIfNotExist(key, value,exp);
		}
		if(null != redisClient)
			redisClient.setIfNotExist(key, value,exp);
	}

	/**
	 * 放入缓存，当该key已存在或网络出错，统一都抛出PlatformException
	 * 
	 * @param prefix
	 *            指定key的前缀
	 * @param key
	 *            放入nkv中的key
	 * @param value
	 *            值
	 * @param exp
	 *            过期时间
	 */
	public void setIfNotExist(String prefix, String key, Object value, int exp) {
		String finalKey = prefix + key;
		setIfNotExist(finalKey, value,exp);
	}

	/**
	 * 序列化
	 * 
	 * @param obj
	 * @return
	 */
	public byte[] serializer(Object obj) {
		return nkvClient.serializer(obj);
	}

	/**
	 * 反序列化
	 * 
	 * @param bytes
	 * @return
	 */
	public Object deserialize(byte[] bytes) {
		return nkvClient.deserialize(bytes);
	}

}
