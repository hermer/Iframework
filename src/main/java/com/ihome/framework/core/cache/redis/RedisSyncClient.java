package com.ihome.framework.core.cache.redis;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SessionCallback;

import com.ihome.framework.core.exception.PlatformException;
import com.netease.backend.nkv.client.NkvClient.NkvOption;

public class RedisSyncClient implements IRedisClient {
	private static final Logger LOG = LoggerFactory.getLogger(RedisSyncClient.class);
	
	private RedisTemplate<String,Object> redisTemplate;
	
	
	public RedisSyncClient() {
		super();
	}



	public RedisSyncClient(RedisTemplate<String, Object> redisTemplate) {
		super();
		this.redisTemplate = redisTemplate;
	}



	/**
	 * 放入缓存
	 * 
	 * @param key
	 *            key
	 * @param value
	 *            值
	 */
	public void set(String key, Object value) {
		try {
			redisTemplate.opsForValue().set(key, value);
		} catch (Exception e) {
			throw new PlatformException("redis的set key value 操作异常",e);
		}
	}

	
	
	/**
	 * 放入缓存
	 * 
	 * @param key
	 *            key
	 * @param exp
	 *            过去时间，单位秒
	 * @param value
	 *            值
	 */
	public void set(String key, Object value, int exp) {
		try {
			redisTemplate.opsForValue().set(key, value, exp, TimeUnit.SECONDS);
		} catch (Exception e) {
			throw new PlatformException("redis的set key value exp 操作异常",e);
		}
		
	}

	/**
	 * 当对应的key不存在时 放入缓存
	 * 如果key存在 参考NKV接口实现 如果key值存在抛 运行时异常
	 * 
	 * @param key
	 *            key
	 * @param value
	 *            值
	 */
	public void setIfNotExist(String key, Object value) {
		try {
			boolean result=redisTemplate.opsForValue().setIfAbsent(key, value);
			if(!result) {
				throw new PlatformException("redis的setIfNotExist key value 操作异常,"+key+"is already exists");
			}
		} catch (Exception e) {
			throw new PlatformException("redis的setIfNotExist key value 操作异常",e);
		}
		
	}
	/**
	 * 当对应的key不存在时 放入缓存
	 * 
	 * @param key
	 *            key
	 * @param value
	 *            值
	 * @param exp
	 *            过期时间，单位秒
	 */
	public void setIfNotExist(String key, Object value, int exp) {
		try {
			boolean result=redisTemplate.opsForValue().setIfAbsent(key, value);
			if(!result) {
				throw new PlatformException("redis的setIfNotExist key value 操作异常,"+key+"is already exists");
			}
			redisTemplate.expire(key, exp, TimeUnit.SECONDS);
		} catch (Exception e) {
			throw new PlatformException("redis的setIfNotExist key value exp 操作异常",e);
		}
	}

	/**
	 * 从缓存中获取对象
	 * 
	 * @param key
	 *            key
	 * @return Object
	 */
	public Object get(String key) {
		try {
			return redisTemplate.opsForValue().get(key);
		} catch (Exception e) {
			throw new PlatformException("redis的get key 操作异常",e);
		}
	}

	/**
	 * 根据key获取对象
	 * 
	 * @param key
	 *            key
	 * @param t
	 *            类型
	 * @param <T>
	 *            类型
	 * @return object
	 */
	public <T> T get(String key, Class<T> t) {
		try {
			return (T)redisTemplate.opsForValue().get(key);
		} catch (Exception e) {
			throw new PlatformException("redis的get key class 操作异常",e);
		}
	}

	/**
	 * 从缓存中删除
	 * 
	 * @param key
	 *            cache key
	 */
	public void delete(String key) {
		try {
			redisTemplate.delete(key);
		} catch (Exception e) {
			throw new PlatformException("redis的delete key 操作异常",e);
		}
	}
	
	
	

	
	@Override
	public void deleteIfNotExists(String key) {
		try {
			final byte[] rawKey = redisTemplate.getStringSerializer().serialize(key);
			Long result=redisTemplate.execute(new RedisCallback<Long>() {
				public Long doInRedis(RedisConnection connection) {
					Long n=connection.del(rawKey);
					return n;
				}
			}, true);
			if(result<=0)
				throw new PlatformException("redis的deleteIfNotExists key 操作异常,不存在key值["+key+"]");
		} catch (Exception e) {
			throw new PlatformException("redis的delete key 操作异常",e);
		}
	}
	

	@Override
	public int incr(final String key, final int value, final int initValue) {
		int result=-1;
        if(!redisTemplate.hasKey(key)) {
        	redisTemplate.opsForValue().set(key, initValue);
        }
        result=Integer.valueOf(redisTemplate.opsForValue().increment(key, value).toString());
        return result;
	}




	public RedisTemplate<String, Object> getRedisTemplate() {
		return redisTemplate;
	}



	public void setRedisTemplate(RedisTemplate<String, Object> redisTemplate) {
		this.redisTemplate = redisTemplate;
	}



	

	
	

}
