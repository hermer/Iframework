package com.ihome.framework.core.lock;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public interface IHomeDistributedLock extends InitializingBean,DisposableBean{

	/**
	 * 获取锁（默认锁失效时间10s） 获取错误 throw DistributedLockException
	 * 
	 * @param key
	 */
	void lock() throws DistributedLockException;
	
	
	boolean tryLock(int timeout, TimeUnit unit);
	

	/**
	 * 获取锁(并发流量控制)
	 * 
	 * @param key
	 * @param timeout
	 * @param unit
	 */
	boolean tryActivesLock(int timeout, TimeUnit unit) throws Exception;
	/**
	 * 解锁（decr actives）
	 * @return
	 * @throws Exception
	 */
	boolean activesUnlock() throws Exception;
	
	/**
	 * 检查锁是否超时, 超时unlock()
	 * 返回true则锁继续持有
	 * @return
	 */
	boolean check();
	
	/**
	 * 检查服务可用
	 * @return
	 */
	boolean ping();
	

	/**
	 * 根据key解锁
	 * 
	 * @param key
	 */
	boolean unlock();
	
	String lockName();

	boolean isLock();

}
