package com.ihome.framework.core.lock;

import com.ihome.framework.core.exception.PlatformException;

public class DistributedLockException extends PlatformException {

	private static final long serialVersionUID = 1L;

	public DistributedLockException() {
	}

	public DistributedLockException(int code, String message) {
		super(code, message);
	}

}
