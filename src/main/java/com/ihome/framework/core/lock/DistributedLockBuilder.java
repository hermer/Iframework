package com.ihome.framework.core.lock;

import java.util.concurrent.TimeUnit;

import com.ihome.framework.core.cache.IHomeCacheClient;
import com.ihome.framework.core.cache.nkv.IHomeNkvClient;

public class DistributedLockBuilder {

	private Factory factory;

	private long exp;

	private TimeUnit unit;

	private String lockName;
	
	private int actives = 1;

	private Object client;

	public enum Factory {
		NKV, ZK
	}

	public DistributedLockBuilder lockName(String lockName) {
		this.lockName = lockName;
		return this;
	}

	public DistributedLockBuilder factory(Factory factory) {
		this.factory = factory;
		return this;
	}

	public DistributedLockBuilder client(Object client) {
		this.client = client;
		return this;
	}

	public DistributedLockBuilder exp(long exp, TimeUnit unit) {
		this.exp = exp;
		this.unit = unit;
		return this;
	}
	
	public DistributedLockBuilder actives(int actives){
	    this.actives = actives;
	    return this;
	}

	public IHomeDistributedLock build() {
		org.springframework.util.Assert.notNull(factory);
		org.springframework.util.Assert.notNull(lockName);
		if (factory == Factory.NKV) {
				return new NkvDistributedLockImpl(lockName, (IHomeNkvClient) client, actives, exp, unit);
		} else {
			throw new UnsupportedOperationException();
		}
	}

}
