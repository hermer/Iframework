/**
 * 
 */
package com.ihome.framework.core.lock;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.DisposableBean;

/**
 * @author zww
 *
 */
public interface IHomeMultiDistributedLock extends DisposableBean{
    
    
    /**
     * 获取锁（如果获取失败将重试）
     * @param lockName
     * @param waitTimeOut
     * @param lockTimeOut
     * @param unit
     * @return
     */
    boolean tryLock(String lockName,int waitTimeOut,int lockTimeOut,TimeUnit unit);

    /**
     * 根据key解锁
     * 
     * @param key
     */
    boolean unlock();
    
    

}
