/**
 * 
 */
package com.ihome.framework.core.lock;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.core.NamedThreadLocal;

import com.ihome.framework.core.lock.DistributedLockBuilder.Factory;

/**
 * @author zww
 *
 */
public class IHomeMultiDistributedLockThreadLocal implements DisposableBean {
    
    
    private final ThreadLocal<IHomeMultiDistributedLock> LOCAL_MULTI_LOCKS  = new NamedThreadLocal<IHomeMultiDistributedLock>("local multi locks");
    
    
    protected void buildLock(Factory factory,Object iHomeNkvClient){
        if(LOCAL_MULTI_LOCKS.get() == null){
            LOCAL_MULTI_LOCKS.set(new HomeMultiDistributedLockImpl(factory, iHomeNkvClient));
        }
    }
    
    protected IHomeMultiDistributedLock currentLock(){
        return LOCAL_MULTI_LOCKS.get();
    }
    
    
    protected void unlock(){
        if(LOCAL_MULTI_LOCKS.get() != null){
            if(LOCAL_MULTI_LOCKS.get().unlock()){
                LOCAL_MULTI_LOCKS.remove();
            }
        }
    }

    /* (non-Javadoc)
     * @see org.springframework.beans.factory.DisposableBean#destroy()
     */
    @Override
    public void destroy() throws Exception {
        if(LOCAL_MULTI_LOCKS.get() != null){
            LOCAL_MULTI_LOCKS.get().destroy();
        }
    }

}
