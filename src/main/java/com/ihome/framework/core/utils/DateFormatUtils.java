/**
 * 
 */
package com.ihome.framework.core.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.time.FastDateFormat;

/**
 * @author zww
 *
 */
public class DateFormatUtils {
    
    
    private static final String DATETIME_FORMAT_PATTERN = "yyyy-MM-dd HH:mm:ss";
    
    public static final FastDateFormat DEFAULT_DATETIME_FORMAT = FastDateFormat.getInstance(DATETIME_FORMAT_PATTERN);
    
    
    public static String format(Date date){
        return DEFAULT_DATETIME_FORMAT.format(date);
    }
    
    
    public static Date parse(String date) throws ParseException{
        return new SimpleDateFormat(DATETIME_FORMAT_PATTERN).parse(date);
    }
    
    
    

}
