/**
 * 
 */
package com.ihome.framework.core.nss;

import com.netease.sentry.javaagent.collector.plugin.user.StatsTool;

/**
 * 监控数据接口的封装
 * 
 * @author zhengxiaohong
 */
public class Monitor {

	/**
	 * 会对key指定的值汇总出总数、最大值、最小值
	 * 
	 * @param key
	 */
	public void increase(String key) {
		increase(key, 1L);
	}

	/**
	 * 会对key指定的值汇总出总数、最大值、最小值
	 * 
	 * @param key
	 * @param value
	 */
	public void increase(String key, long value) {
		StatsTool.onIntegerKey1Value1Stats(key, value);
	}

	/**
	 * 针对metric和key的组合，对value1和value2都进行汇总，对string进行抽样
	 * 
	 * @param metric
	 * @param key
	 * @param value1
	 * @param value2
	 * @param string
	 */
	public void increase(String metric, String key, long value1, long value2, String string) {
		StatsTool.onIntegerKey2Value2Stats(metric, key, value1, value2, string);
	}

}
