package com.ihome.framework.core.sftp;

/**
 * User: William Cheng
 * Create Time: 2018/4/9 17:32
 * Description:
 */
public class SftpClientServiceTest {

    public static void main(String[] args) {

        String remoteOldPath = "/shangpt_test/spt_receipt/20180409";
        String remoteNewPath = "/shangpt_test/receipt/20180409";
        SFtpConfig sftpConfig = new SFtpConfig();
        sftpConfig.setHost("test-ftp.zsyjr.com");
        sftpConfig.setPort(22);
        sftpConfig.setUsername("app_test");
        sftpConfig.setPrivatekey("D:\\apps\\xshell\\conf\\test.private");

        SFtpChannelFactory factory = new SFtpChannelFactory();
        SFtpClientService sFtpClientService = new SFtpClientService(factory);
        sFtpClientService.renameWithConfig(remoteNewPath, remoteOldPath, sftpConfig);
    }
}