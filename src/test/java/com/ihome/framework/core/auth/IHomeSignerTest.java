package com.ihome.framework.core.auth;

import org.junit.Assert;
import org.junit.Test;

public class IHomeSignerTest {
    
    @Test
    public void testSignAndVerify() {
        String privateKey = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBANcZ8tNxiISOhfkUfaiDLuUd1zzdPCoX26fD8QyxXMt/Qo4k9K54ThEi/yCqiy0c94mbtpd9TZmzekOv71f2K3Y/ua0EXjGZIM6TM444JPGs7A1oHuZMp0yTbJzCi50CPwcBWpllSxiNjcJClGSxeGwUAMQC4wYzpdqwNoEQAvx5AgMBAAECgYAu7ZBWpRxdnyKHdvr7OrOOdPYyZynrHQ1lCpCabk+Kbhc9H77qGYEep/31SH/YtcsjkH9DiajWcZupDMcw75oHaev2YYH8IY/njuxGXRxByDuU8KvFn1L2ERK6gzKOPLfasZXRf8jmMuvEzUnM4YvpK3QHHkf1QY/He5BKVb2qoQJBAPGtXNCAbl3x00FGc7kMQ+zunm1JCRpcNJHUibAKEMqSen2RuoVk5xm908AVMhxTqxvdV4ETyrIp3HyUpjU7YLUCQQDj2WMejaDKdJxNqvoHTbI3o9FhBaoBZ0Au/GItF9ldDqQNLObwVBeHnYRsiPhwrPWMcEvFnOSa/24LrFlL3Xs1AkEAznAkwSGDpRHFP5Pbk+zrNL8bawShFvg+G3duaYYfk7oNkadiqUFouq9Q6lnwpQu/NJ0cevCaCxgA9BXR59ShpQJAXsJyLqrITGri9oo9Ifx2Oh2vXqJ0adD/KIteaVKZ29HS+IfMI/dVMdmY0WrFqFbGaHnriWwOx0ekBMbQ/HRIYQJBAIDidFzsoV5ZgA2J8GQr//AxfSLAb7Q4JqEucqgeif6/h8dPK+xW2xIL/IMOxG/zg7CJoW/fmlU98qi8jufEcEU=";
        String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDXGfLTcYiEjoX5FH2ogy7lHdc83TwqF9unw/EMsVzLf0KOJPSueE4RIv8gqostHPeJm7aXfU2Zs3pDr+9X9it2P7mtBF4xmSDOkzOOOCTxrOwNaB7mTKdMk2ycwoudAj8HAVqZZUsYjY3CQpRksXhsFADEAuMGM6XasDaBEAL8eQIDAQAB";
        String content = "test+hahaha+wohahah+wowowow!";
        String sign = IHomeSigner.signByRsa(content, privateKey);
        boolean pass = IHomeSigner.verifyByRsa(content, publicKey, sign);
        Assert.assertTrue(pass);
    }

}
