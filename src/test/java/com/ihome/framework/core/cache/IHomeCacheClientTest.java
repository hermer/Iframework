/**
 * 
 */
package com.ihome.framework.core.cache;

import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.redis.core.RedisTemplate;

import com.beust.jcommander.internal.Lists;
import com.ihome.framework.core.cache.nkv.IHomeNkvClient;
import com.ihome.framework.core.commons.BaseConst;
import com.ihome.framework.core.serializer.HessianSerializer;

import junit.framework.Assert;

public class IHomeCacheClientTest {
	

    static IHomeNkvClient cacheClient;
   
    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    	
    	
//    	ClassPathXmlApplicationContext context=new ClassPathXmlApplicationContext("framework-core/ihome-framework-nkv-redis.xml");
//    	ClassPathXmlApplicationContext context=new ClassPathXmlApplicationContext("framework-core/ihome-framework-nkv.xml");
    	ClassPathXmlApplicationContext context=new ClassPathXmlApplicationContext("framework-core/ihome-framework-redis.xml");
    	
    	
    	
    	cacheClient=(IHomeNkvClient) context.getBean("iHomeNkvClient");
    }
    
    @AfterClass
    public static void setUpAfterClass() throws Exception {
    	
    	Thread.sleep(1000 * 60 * 60);
    }
    	
    @Test
    public void testSetString() {
    	String key="demo_set1";
    	String value="demo_set_value1";
    	cacheClient.set(key, value);
    }
    
    static class Person implements java.io.Serializable{
    	private String name;
    	private int age;
		public String getName() {
			return name;
		}
		public int getAge() {
			return age;
		}
		public void setName(String name) {
			this.name = name;
		}
		public void setAge(int age) {
			this.age = age;
		}
    }
    
    @Test
    public void testSetObj() {
    	String key="demo_set_obj1";
    	Person p=new Person();
    	p.setName("name");
    	p.setAge(123);
    	cacheClient.set(key, p);
    }
    
    @Test
    public void testSetObjList() {
    	String key="demo_set_obj_list";
    	Person p=new Person();
    	p.setName("name");
    	p.setAge(123);
    	List<Person> personList=Lists.newArrayList();
    	personList.add(p);
    	personList.add(p);
    	cacheClient.set(key, personList);
    }
    
    
    
    @Test
    public void testGetObj() {
    	String key="demo_set_obj";
    	Person p=cacheClient.get(key, Person.class);
    	
    	Assert.assertEquals(123, p.getAge());
    }
    
    @Test
    public void testSetExpire() {
    	String key="demo_set_exp";
    	String value="demo_set_value";
    	cacheClient.set(key,value,5);
    }
    
    @Test
    public void testSetIfNotExist() {
    	String key="demo_set_notexist";
    	String value="demo_set_notexist_value1";
    	cacheClient.setIfNotExist(key, value);
    }
}
