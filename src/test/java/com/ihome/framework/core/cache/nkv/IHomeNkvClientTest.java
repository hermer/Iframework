/**
 * 
 */
package com.ihome.framework.core.cache.nkv;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.security.SecureRandom;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.BeforeClass;
import org.junit.Test;

import com.ihome.framework.core.commons.BaseConst;
import com.ihome.framework.core.exception.PlatformException;
import com.ihome.framework.core.lock.DistributedLockBuilder;
import com.ihome.framework.core.lock.DistributedLockBuilder.Factory;
import com.ihome.framework.core.lock.IHomeDistributedLock;
import com.ihome.framework.core.lock.NkvDistributedLockImpl;
import com.ihome.framework.core.serializer.HessianSerializer;
import com.ihome.framework.core.utils.CoreUtils;
import com.netease.backend.nkv.client.NkvClient.NkvOption;
import com.netease.backend.nkv.client.Result;
import com.netease.backend.nkv.client.impl.DefaultNkvClient;

import junit.framework.Assert;

/**
 * @author daniel
 * 
 */
public class IHomeNkvClientTest {

    static IHomeNkvClient nkvClient;
    static String master = "nkv.wjjr.cc:8888";
    static String slave = "nkv.wjjr.cc:8888";
    static String group = "group_1";
    static String namespace = "online1";
    long timeout = 5000;
    static HessianSerializer<Object> serializer = new HessianSerializer<Object>();

    public static final String PREFIX = "nkvTest";
    public static final String NKV_TEST_1 = "nkvTest1";
    public static final String NKV_TEST_2 = "NKV_TEST_2";

    private SecureRandom random = new SecureRandom(); 

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        nkvClient = new IHomeNkvClient(master, slave, group, namespace);
        nkvClient.setSerializer(serializer);
        nkvClient.setTimeout(30 * BaseConst.SECOND);
    }
    //
    //	/**
    //	 * Test method for
    //	 * {@link com.ihome.framework.core.cache.nkv.IHomeNkvClient#IHomeNkvClient(java.lang.String, java.lang.String, java.lang.String, java.lang.String)}
    //	 * .
    //	 */
    //	@Test
    //	public void testIHomeNkvClientStringStringStringString() {
    //		IHomeNkvClient client = new IHomeNkvClient(master, slave, group, namespace);
    //		Assert.assertNotNull(client);
    //	}
    //
    //	/**
    //	 * Test method for
    //	 * {@link com.ihome.framework.core.cache.nkv.IHomeNkvClient#IHomeNkvClient(java.lang.String, java.lang.String, java.lang.String, java.lang.String, long)}
    //	 * .
    //	 */
    //	@Test
    //	public void testIHomeNkvClientStringStringStringStringLong() {
    //		IHomeNkvClient client = new IHomeNkvClient(master, slave, group, namespace, timeout);
    //		Assert.assertNotNull(client);
    //	}
    //
    	@Test
    	public void testIncrVersion() {
    		short x = Short.MAX_VALUE - 10;
    		for(int i=0; i<20; i++) {
    			x = nkvClient.incrVersion(x);
    			System.out.println(x);
    		}        
    		assertEquals(x, (short)11);
    	}
    	
    	
    //
    //
    //	/**
    //	 * Test method for
    //	 * {@link com.ihome.framework.core.cache.nkv.IHomeNkvClient#set(java.lang.String, java.lang.Object)}
    //	 * .
    //	 */
    //	@Test
    //	public void testSetStringObject() {
    //		String key = NKV_TEST_1;
    //		String value = UUID.randomUUID().toString();
    //		nkvClient.set(key, value);
    //
    //		Object obj = nkvClient.get(key);
    //		assertTrue(obj instanceof String);
    //		assertEquals(value, obj);
    //	}
    //
    //	/**
    //	 * Test method for
    //	 * {@link com.ihome.framework.core.cache.nkv.IHomeNkvClient#set(java.lang.String, java.lang.Object, int)}
    //	 * .
    //	 */
    //	@Test
    //	public void testSetStringObjectInt() {
    //		String key = NKV_TEST_2;
    //		String value = UUID.randomUUID().toString();
    //		int exp = 1;
    //		nkvClient.set(key, value, exp);
    //		try {
    //			Thread.sleep(2000);
    //		} catch (InterruptedException e) {
    //		}
    //		Object obj = nkvClient.get(key);
    //		assertNull(obj);
    //	}
    //
    //	/**
    //	 * Test method for
    //	 * {@link com.ihome.framework.core.cache.nkv.IHomeNkvClient#set(java.lang.String, java.lang.String, java.lang.Object)}
    //	 * .
    //	 */
    //	@Test
    //	public void testSetStringStringObject() {
    //		String prefix = PREFIX;
    //		String key = NKV_TEST_1;
    //		String value = UUID.randomUUID().toString();
    //		nkvClient.set(prefix, key, value);
    //
    //		Object obj = nkvClient.get(prefix, key);
    //		assertTrue(obj instanceof String);
    //		assertEquals(value, obj);
    //	}
    //
    //	public void testSetStringStringObjectNkvOption(){
    //		String prefix = PREFIX;
    //		String key = NKV_TEST_1;
    //		String value = UUID.randomUUID().toString();
    //		int exp = 1;
    //		NkvOption opt = new NkvOption(5000, (short)1, exp);
    //		nkvClient.set(prefix, key, value, opt);
    //		try {
    //			Thread.sleep(2000);
    //		} catch (InterruptedException e) {
    //		}
    //		Object obj = nkvClient.get(prefix, key);
    //		assertNull(obj);
    //	}
    //
    //	/**
    //	 * Test method for
    //	 * {@link com.ihome.framework.core.cache.nkv.IHomeNkvClient#set(java.lang.String, java.lang.String, java.lang.Object, int)}
    //	 * .
    //	 */
    //	@Test
    //	public void testSetStringStringObjectInt() {
    //		String prefix = UUID.randomUUID().toString().substring(12);
    //		String key = UUID.randomUUID().toString();
    //		String value = UUID.randomUUID().toString();
    //		int exp = 1;
    //		nkvClient.set(prefix, key, value, exp);
    //		try {
    //			Thread.sleep(2000);
    //		} catch (InterruptedException e) {
    //		}
    //		Object obj = nkvClient.get(prefix, key);
    //		assertNull(obj);
    //	}
    //
    //	/**
    //	 * Test method for
    //	 * {@link com.ihome.framework.core.cache.nkv.IHomeNkvClient#get(java.lang.String)}
    //	 * .
    //	 */
    //	@Test
    //	public void testGetString() {
    //		testSetStringObject();
    //	}
    //
    //	/**
    //	 * Test method for
    //	 * {@link com.ihome.framework.core.cache.nkv.IHomeNkvClient#get(java.lang.String, java.lang.String)}
    //	 * .
    //	 */
    //	@Test
    //	public void testGetStringString() {
    //		testSetStringStringObject();
    //	}
    //
    //	/**
    //	 * Test method for
    //	 * {@link com.ihome.framework.core.cache.nkv.IHomeNkvClient#get(java.lang.String, java.lang.Class)}
    //	 * .
    //	 */
    //	@Test
    //	public void testGetStringClassOfT() {
    //		String key = UUID.randomUUID().toString();
    //		Double value = random.nextDouble();
    //		nkvClient.set(key, value);
    //
    //		Double cacheValue = nkvClient.get(key, Double.class);
    //		assertEquals(value, cacheValue);
    //	}
    //
    //	/**
    //	 * Test method for
    //	 * {@link com.ihome.framework.core.cache.nkv.IHomeNkvClient#get(java.lang.String, java.lang.String, java.lang.Class)}
    //	 * .
    //	 */
    //	@Test
    //	public void testGetStringStringClassOfT() {
    //		String prefix = UUID.randomUUID().toString().substring(12);
    //		String key = UUID.randomUUID().toString();
    //		Double value = random.nextDouble();
    //		nkvClient.set(prefix, key, value);
    //
    //		Double cacheValue = nkvClient.get(prefix, key, Double.class);
    //		assertEquals(value, cacheValue);
    //	}
    //
    //	/**
    //	 * Test method for
    //	 * {@link com.ihome.framework.core.cache.nkv.IHomeNkvClient#delete(java.lang.String)}
    //	 * .
    //	 */
    //	@Test
    //	public void testDelete() {
    //		String key = UUID.randomUUID().toString();
    //		Double value = random.nextDouble();
    //		nkvClient.set(key, value);
    //		nkvClient.delete(key);
    //
    //		Double cacheValue = nkvClient.get(key, Double.class);
    //		assertNull(cacheValue);
    //	}
    //
    //	/**
    //	 * Test method for
    //	 * {@link com.ihome.framework.core.cache.nkv.IHomeNkvClient#getNkvClient()}.
    //	 */
    //	@Test
    //	public void testGetNkvClient() {
    //		DefaultNkvClient nkv = nkvClient.getNkvClient();
    //		assertNotNull(nkv);
    //	}
    //
    //	@Test
    //	public void justTest() {
    //		/* String prefix = "test";
    //        String key = "zxhtest";
    //        Object value = 100;*/
    //		NkvOption opt = new NkvOption(500000, (short) 3);
    //		Assert.assertNotNull(opt);
    //		//        nkvClient.set(prefix, key, value);
    //	}
    //
    //	/**
    //	 * 测试一下nkv的过期
    //	 * 经测试：
    //        1、get是不会重新计时的
    //        2、第二次set不传expire这个参数，该key就不会过期了
    //	 */
    //	@Test
    //	public void testExpire() {
    //		String prefix = "test";
    //		String key = "testExpire";
    //		nkvClient.set(prefix, key, 123, 3);
    //		CoreUtils.sleep(2000);
    //		NkvResult<Object> nkvResult = nkvClient.getResult(prefix, key);
    //		Result<Object> result = nkvResult.getOriginResult();
    //		System.out.println(nkvResult.getResult());
    //		System.out.println(result.getExpire());
    //
    //		nkvClient.set(prefix, key, new Integer(1234));
    //		CoreUtils.sleep(2000);
    //		nkvResult = nkvClient.getResult(prefix, key);
    //		result = nkvResult.getOriginResult();
    //		System.out.println(nkvResult.getResult());
    //		System.out.println(result.getExpire());
    //	}
    //
    //
    //
    //	@Test
    //	public void testLock(){
    //		String key = "testExpire";
    //		IHomeDistributedLock distributedLock = new DistributedLockBuilder().factory(Factory.NKV).lockName(key).exp(1, TimeUnit.SECONDS).client(nkvClient).build();
    //		Assert.assertTrue(!distributedLock.isLock());
    //		distributedLock.lock();
    //		Assert.assertTrue(distributedLock.isLock());
    //		distributedLock.unlock();
    //		Assert.assertTrue(!distributedLock.isLock());
    //		distributedLock.tryLock(10, TimeUnit.SECONDS);
    //		Assert.assertTrue(distributedLock.isLock());
    //		distributedLock.unlock();
    //	}
    //	
    //	
//    @Test
//    public void testThreadLock_Unit1(){
//        String key = "testExpire";
//        final IHomeDistributedLock distributedLock = new DistributedLockBuilder().factory(Factory.NKV).lockName(key).exp(10, TimeUnit.SECONDS).client(nkvClient).build();
//        ExecutorService executorService = Executors.newCachedThreadPool();
//        for (int i = 0; i < 3; i++) {
//            executorService.execute(new Runnable() {
//
//                @Override
//                public void run() {
//                    distributedLock.tryLock(10, TimeUnit.SECONDS);
//                    try {
//                        Thread.sleep(ThreadLocalRandom.current().nextLong(1, 300l));
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }finally{
//                        distributedLock.unlock();
//                    } // doing
//                }
//            });
//        }
//        try {
//            executorService.awaitTermination(10000l, TimeUnit.SECONDS);
//        } catch (InterruptedException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//
//    }
    	
    	
 	/**
	 * Test method for
	 * {@link com.ihome.framework.core.cache.nkv.IHomeNkvClient#setIfNotExist(java.lang.String,java.lang.Object)}
	 * .
	 */
	@Test
	public void testSetIfNotExists() {
		String key="test_key";
		String value="test_val";
		System.out.println("key:"+key+",value:"+value);
		nkvClient.setIfNotExist(key, value);
		Object obj = nkvClient.get(key);
		assertEquals(value, obj);
	}
	
	@Test
	public void testSet() {
		String key="test_key";
		String value="test_val";
		System.out.println("key:"+key+",value:"+value);
		nkvClient.set(key,value);
	}
	
	
	@Test
	public void testGetResult() {
		String key="test_key";
		String value="test_val";
		System.out.println("key:"+key+",value:"+value);
		Object obj = nkvClient.getResult("", key);
		System.out.println("finishing......");
		assertNotNull( obj);
	}
	
	@Test
	public void testSetResult() {
		String key="test_key_1";
		String value="test_val";
		System.out.println("key:"+key+",value:"+value);
		Object obj = nkvClient.setResult("",key,value,new NkvOption(100));
		System.out.println("finishing......");
		assertNotNull( obj);
	}

	@Test
	public void testSetCount() {
		String key="test_set_count";
		Object obj = nkvClient.setCount("","test_set_count", 1);
		System.out.println("finishing......");
		assertNotNull( obj);
	}
	
	@Test
	public void testGetCount() {
		String key="test_set_count";
		Object obj = nkvClient.getCount("","test_set_count");
		System.out.println("finishing......");
		assertNotNull( obj);
	}
	
	
	@Test
	public void testDecr() {
		String key="test_decr";
		int result = nkvClient.decr("", key, 1, 100);
		System.out.println("finishing......");
	
	}
	

    @Test
    public void testThreadActivesLock_Unit1() throws Exception{

        String key = "testExpire";

        final NkvDistributedLockImpl distributedLock = (NkvDistributedLockImpl) new DistributedLockBuilder().factory(Factory.NKV)
                .lockName("key3").actives(5).client(nkvClient).build();

        ExecutorService executorService = Executors.newCachedThreadPool();


        final AtomicInteger integer  = new AtomicInteger(0);

        for (int i = 0; i < 30; i++) {
            executorService.execute(new Runnable() {
                // 0/2
                @Override
                public void run() {
                    try{
                        if(distributedLock.tryActivesLock(20, TimeUnit.SECONDS)){
                            // avg 
                            Thread.sleep(ThreadLocalRandom.current().nextLong(1, 1000l)); // doing
                            System.out.println(Thread.currentThread().getName()+"-"+integer.incrementAndGet());
                            System.out.println(distributedLock.getActiveThreadHashList().toString());
                            distributedLock.activesUnlock();
                        }else{
                            System.err.println("pass");
                        }
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        Thread.sleep(4000000l);

        distributedLock.destroy();

    }



}
