package com.ihome.framework.core.validator;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

public class TestReq {

    @NotNull
    private Integer customerId;

    @NotBlank
    private String merchantNo;
}
