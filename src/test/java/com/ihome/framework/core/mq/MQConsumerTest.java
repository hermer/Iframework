package com.ihome.framework.core.mq;

import java.io.UnsupportedEncodingException;

import com.netease.cloud.nqs.client.Message;
import com.netease.cloud.nqs.client.consumer.MessageHandler;

public class MQConsumerTest {

    public static void main(String[] args) {
        reConnectTest();   
    }

    /**
     * 重连测试
     * @param args
     */
    public static void reConnectTest() {
        final MQConfig mqConfig = new MQConfig();
        mqConfig.setHost("10.211.193.137");
        mqConfig.setPort(5672);
        mqConfig.setUsername("guest");
        mqConfig.setPassword("guest");
        mqConfig.setExchange("account-server");

        MQConsumerConfig mqcc = new MQConsumerConfig();
        mqcc.setPrefetchCount(100);
        final String queueName = "FUND_MIGRATE";

        MQMultiThreadConsumer mqc = new MQMultiThreadConsumer(mqConfig, mqcc);

        //        MQConsumer mqc = new MQConsumer(mqConfig, mqcc);
        mqc.init();
        for (int i = 0; i < 10; i++) {
            mqc.consumeMessage(queueName, new MessageHandler() {
                @Override
                public boolean handle(Message mess) {
                    try {
                        System.out.println(new String(mess.getBody(), "UTF-8"));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    return true;
                }
            });
        }

        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    Thread.sleep(2000L);
                } catch (InterruptedException e) {
                }
                for (int j = 0; j < 3; j++) {
                    MQProducerConfig mpc = new MQProducerConfig();
                    mpc.setRequireConfirm(true);
                    mpc.setConfirmTimeout(3000);

                    final MQProducer producer = new MQProducer(mqConfig, mpc);
                    producer.init();
                    for (int i = 0; i < 10; i++) {
                        producer.sendMessage("justtest" + i, queueName);
                    } 
                }
                
            }
        }).start();



        try {
            Thread.sleep(50000L);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        //        mqc.consumeMessage("framework_usage_2", new MessageHandler() {
        //            @Override
        //            public boolean handle(Message mess) {
        //                try {
        //                    System.out.println(new String(mess.getBody(), "UTF-8"));
        //                } catch (UnsupportedEncodingException e) {
        //                    e.printStackTrace();
        //                }
        //                return false;
        //            }
        //        });
        //        
        //        mqc.consumeMessage("framework_usage_3", new MessageHandler() {
        //            @Override
        //            public boolean handle(Message mess) {
        //                try {
        //                    System.out.println(new String(mess.getBody(), "UTF-8"));
        //                } catch (UnsupportedEncodingException e) {
        //                    e.printStackTrace();
        //                }
        //                return false;
        //            }
        //        });
    }

}
