package com.ihome.framework.core.mq;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sun.net.InetAddressCachePolicy;

import com.ihome.framework.core.commons.BaseConst;
import com.ihome.framework.core.utils.CoreUtils;

public class MQProducerTest {

    private final static Logger logger = LoggerFactory.getLogger(MQProducerTest.class);

    public static void main(String[] args) {
        concurrencTest();
    }

    /**
     * 生产者并发测试
     */
    public static void concurrencTest() {
        final MQConfig mqConfig = new MQConfig();
        mqConfig.setHost("10.166.224.34");
        mqConfig.setPort(5672);
        mqConfig.setUsername("guest");
        mqConfig.setPassword("guest");
        mqConfig.setExchange("framework");
        // MQConfig mqConfig = new MQConfig();
        // mqConfig.setHost("mq.wjjr.cc");
        // mqConfig.setPort(5672);
        // mqConfig.setUsername("appops");
        // mqConfig.setPassword("appops");
        // mqConfig.setExchange("channels");

        final MQProducerConfig mpc = new MQProducerConfig();
        mpc.setRequireConfirm(true);
        mpc.setConfirmTimeout(3000);

        final MQProducer producer = new MQProducer(mqConfig, mpc);
        producer.init();
        for (int i = 0; i < 10; i++) {
            producer.sendMessage("justtest" + i, "framework_usage_xxxxx");
        }

        // while(true) {
        // try {
        // InetAddress addr = InetAddress.getByName("mq.wjjr.cc");
        // System.out.println(addr.getHostAddress());
        // } catch (UnknownHostException e) {
        // e.printStackTrace();
        // }
        // CoreUtils.sleep(BaseConst.SECOND * 15);
        // }
        // while(true) {
        // final MQProducer producer = new MQProducer(mqConfig, mpc);
        // producer.init();
        // producer.sendMessage("justtest", "framework_usage");
        // CoreUtils.sleep(BaseConst.SECOND * 15);
        // producer.destory();
        // }
        // for (int i = 0; i < 30; i++) {
        // Thread t = new Thread() {
        // public void run() {
        // CoreUtils.sleep(BaseConst.SECOND * 30);
        // while (true) {
        // producer.sendMessage("justtest", "framework_usage");
        // CoreUtils.sleep(BaseConst.SECOND * 1);
        // logger.info("send message");
        // }
        // }
        // };
        // t.start();
        // }
    }

}
