package com.ihome.framework.core.utils;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

public class FreemarkerUtilsTest {

    @Test
    public void testGetTemplateStr() {
        Assert.fail("Not yet implemented");
    }

    @Test
    public void testGetTemplateStrByContent() {
        String name = "demoFtl";
        String content = "hello , ${account}";
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("account", "framework");
        for (int i = 0; i < 10; i++) {
            long start = System.currentTimeMillis();
            String finalContent = FreemarkerUtils.getTemplateStrByContent(name, content, data);
            long time = System.currentTimeMillis() - start;
            Assert.assertNotNull(finalContent);
            System.out.println(time + " " + finalContent);
            try{
                Thread.sleep(1000);
            }catch(Exception ex){}
        }
    }

    @Test
    public void testGetContentIgnoreNull() {
        String name = "demoFtl";
        String content = "hello , ${account}, ${test}, ${date}";
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("account", "framework");
        for (int i = 0; i < 10; i++) {
            long start = System.currentTimeMillis();
            String finalContent = "";
            try {
                finalContent = FreemarkerUtils.getContentIgnoreNull(name, content, data);
            } catch (Exception e) {
                e.printStackTrace();
            }
            long time = System.currentTimeMillis() - start;
            Assert.assertNotNull(finalContent);
            System.out.println(time + " " + finalContent);
            try{
                Thread.sleep(1000);
            }catch(Exception ex){}
        }
    }

    @Test
    public void testGetContentWithExp() {
        String name = "demoFtl";
        String content = "hello , ${account}, ${test}, ${date}  , ${csnloan_applyTime?string('yyyy年-MM月-dd日')}";
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("account", "framework");
        String finalContent = null;
        try {
            finalContent = FreemarkerUtils.getContentWithExp(name, content, data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(finalContent);
    }

}
