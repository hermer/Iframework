package com.ihome.framework.core.mdc;

import java.util.concurrent.Future;

public interface AsyncService {

	Future<String> doWork();
}
