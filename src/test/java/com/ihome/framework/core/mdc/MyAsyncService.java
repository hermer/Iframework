package com.ihome.framework.core.mdc;

import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.MDC;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

@Component
public class MyAsyncService implements AsyncService {

	private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(MyAsyncService.class);
	
	@Override
	@Async
	public Future<String> doWork() {
		LOG.info("Doing work");
		LOG.info("Finished doing work");
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new AsyncResult<String>(MDC.get("trackingId"));
	}
}
