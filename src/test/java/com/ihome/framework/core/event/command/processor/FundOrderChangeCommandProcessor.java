/**
 * 
 */
package com.ihome.framework.core.event.command.processor;

import org.springframework.stereotype.Component;

import com.ihome.framework.core.event.IHomeEventBusHandler.EventBusWorkHandler;
import com.ihome.framework.core.event.command.AbstractCommandProcessor;
import com.ihome.framework.core.event.command.AccountWaterInsertCommand;
import com.ihome.framework.core.event.command.CommandEventBusSpringBeanRegister.CommandEvent;
import com.ihome.framework.core.event.command.handler.SimpleCommandEventHandler;
import com.ihome.framework.core.event.command.CommandProcessor;
import com.ihome.framework.core.event.command.FundOrderChangeCommand;
import com.ihome.framework.core.event.command.FundOrderInsertCommand;
import com.ihome.framework.core.event.command.SimpleCommandEventBusHandler;

/**
 * @author zww
 *
 */
@Component
public class FundOrderChangeCommandProcessor extends AbstractCommandProcessor<FundOrderChangeCommand> implements CommandProcessor<FundOrderChangeCommand> {


}
