/**
 * 
 */
package com.ihome.framework.core.event.handler;

import org.springframework.stereotype.Component;

//import com.ihome.framework.core.event.Account;
import com.ihome.framework.core.event.PayoutDataEvent;
import com.ihome.framework.core.event.SpringTestCase;
import com.ihome.framework.core.event.IHomeEventBusHandler.EventBusWorkHandler;
import com.ihome.framework.core.event.command.FundOrderUpdateStatusCommand;

/**
 * @author zww
 *
 */
@Component
public class PayoutDataReceiveHandler extends EventBusWorkHandler<PayoutDataEvent>{
    /* (non-Javadoc)
     * @see com.ihome.framework.core.event.IHomeEventBusHandler#onEvent(com.ihome.framework.core.event.IHomeEventBusHandler.EventDto)
     */
    @Override
    public void handler(PayoutDataEvent e,long sequence,boolean endOfBatch) throws Exception {
        long threadId = Thread.currentThread().getId(); // 获取当前线程id
        String accountNo = e.getAccountNo();
        Integer money = e.getMoney();
        e.addCommand(new FundOrderUpdateStatusCommand(e.getRequestId(),"SUCCESS"));
        LOGGER.info(String.format("UUID %s | Thread Id %s 清结算响应成功[account:%s|status:SUCCESS] ........",e.getRequestId(),threadId, ""));
    }
}

