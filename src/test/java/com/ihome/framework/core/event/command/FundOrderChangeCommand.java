/**
 * 
 */
package com.ihome.framework.core.event.command;

import com.ihome.framework.core.event.command.buffer.CommandBuffer.SqlCommandDto;

/**
 * @author zww
 *
 */
public abstract class FundOrderChangeCommand extends SqlCommandDto{

    /**
     * @param requestId
     */
    public FundOrderChangeCommand(String requestId) {
        super(requestId);
    }

}
