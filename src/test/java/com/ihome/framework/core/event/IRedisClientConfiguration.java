/**
 * 
 */
package com.ihome.framework.core.event;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ihome.framework.core.cache.redis.IHomeRedisClient;

import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * @author zww
 *
 */
@Configuration
public class IRedisClientConfiguration {
    
    
    
    
    
    @Bean
    public JedisPoolConfig jedisPoolConfig()
    {
      /*  JedisPoolConfig jedisPoolConfig=new JedisPoolConfig();
        jedisPoolConfig.setMaxIdle(500);
        jedisPoolConfig.setMaxTotal(500);
        jedisPoolConfig.setMaxWaitMillis(30000);
        jedisPoolConfig.setTimeBetweenEvictionRunsMillis(5000);
        jedisPoolConfig.setMinEvictableIdleTimeMillis(5000);
        jedisPoolConfig.setTestOnBorrow(true);
        jedisPoolConfig.setMinIdle(300);*/
        JedisPoolConfig jedisPoolConfig=new JedisPoolConfig();
        jedisPoolConfig.setMaxTotal(200); 
        jedisPoolConfig.setMaxIdle(100); 
        jedisPoolConfig.setMinIdle(100); 
        jedisPoolConfig.setMaxWaitMillis(3 * 1000); 
        jedisPoolConfig.setTestOnBorrow(true); 
        jedisPoolConfig.setTestOnReturn(true); 
        jedisPoolConfig.setTestWhileIdle(true); 
        jedisPoolConfig.setMinEvictableIdleTimeMillis(500); 
        jedisPoolConfig.setSoftMinEvictableIdleTimeMillis(1000); 
        jedisPoolConfig.setTimeBetweenEvictionRunsMillis(1000); 
        jedisPoolConfig.setNumTestsPerEvictionRun(100); 
        return jedisPoolConfig;
    }
    
    @Bean
    public IHomeRedisClient homeRedisClient(){
        return new IHomeRedisClient("127.0.0.1:6379", "test", "foobared", false, 10000, 10000, 100, jedisPoolConfig());
    }


}
