/**
 * 
 */
package com.ihome.framework.core.event.command.processor;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ihome.framework.core.event.IHomeEventBusHandler.EventBusWorkHandler;
import com.ihome.framework.core.event.command.AbstractCommandProcessor;
import com.ihome.framework.core.event.command.AccountChangeCommand;
import com.ihome.framework.core.event.command.CommandEventBusSpringBeanRegister.CommandEvent;
import com.ihome.framework.core.event.command.buffer.AccountChangeCommandBuffer;
import com.ihome.framework.core.event.command.buffer.CommandBuffer;
import com.ihome.framework.core.event.command.CommandProcessor;
import com.ihome.framework.core.event.command.FrozenAccountCommand;
import com.ihome.framework.core.event.command.SimpleCommandEventBusHandler;
import com.ihome.framework.core.event.command.handler.SimpleCommandEventHandler;

/**
 * @author zww
 *
 */
@Component
public class AccountChangeCommandProcessor extends AbstractCommandProcessor<AccountChangeCommand> implements CommandProcessor<AccountChangeCommand>{
    
   


}
