/**
 * 
 */
package com.ihome.framework.core.event.command;

import org.springframework.stereotype.Component;

import com.ihome.framework.core.event.command.buffer.CommandBuffer.SqlCommandDto;

/**
 * @author zww
 *
 */
public class FundOrderInsertCommand extends FundOrderChangeCommand{
    
    private Long amount;
    
    private String accountNo;

    /**
     * @param requestId
     */
    public FundOrderInsertCommand(String requestId,Long amount,String accountNo) {
        super(requestId);
        this.accountNo = accountNo;
        this.amount = amount;
    }

    /* (non-Javadoc)
     * @see com.ihome.framework.core.event.command.buffer.CommandBuffer.SqlCommandDto#toSql()
     */
    @Override
    public String toSql() {
        return new StringBuffer("INSERT INTO FUND_ORDER_INFO VALUES ("+requestId+","+accountNo+", "+amount+",PRE_SEND,....)").toString();
    }

}
