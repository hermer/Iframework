/**
 * 
 */
package com.ihome.framework.core.event;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.ihome.framework.core.event.IHomeEventBusHandler;
import com.ihome.framework.core.event.group.IHomeEventBusGroupRegister;
import com.ihome.framework.core.event.handler.PayoutDataInDbHandler;
import com.ihome.framework.core.event.handler.PayoutDataReceiveHandler;
import com.ihome.framework.core.event.handler.PayoutDataTallyHandler;
import com.ihome.framework.core.event.handler.PayoutDataToClearHandler;
import com.lmax.disruptor.BlockingWaitStrategy;
import com.lmax.disruptor.TimeoutBlockingWaitStrategy;
import com.lmax.disruptor.dsl.ProducerType;

/**
 * @author zww
 *
 */
@Component
public  class PayoutEventBusHandler extends IHomeEventBusHandler<PayoutDataEvent>{

    public PayoutEventBusHandler() throws Exception{
        super(2048,ProducerType.MULTI);
    }
    /* (non-Javadoc)
     * @see com.ihome.framework.core.event.IHomeEventBusHandler#handleEventException(java.lang.Throwable, long, com.ihome.framework.core.event.IHomeEventBusHandler.EventDto)
     */
    @Override
    public void handleEventException(Throwable ex, long sequence, PayoutDataEvent event) {
        LOGGER.error(String.format("UUID %s sequence %s | Thread Id %s error %s ........",event.getRequestId(),sequence,Thread.currentThread().getId(),ex.getMessage()),ex);
        event.setSuccess(false);
        event.setErrorMessage(ex.getMessage());
    }

    /* (non-Javadoc)
     * @see com.ihome.framework.core.event.IHomeEventConsumer#buildEventsWith(com.ihome.framework.core.event.IHomeEventConsumer, org.springframework.context.ApplicationContext)
     */
    @Override
    public void buildEventsWith(IHomeEventConsumer<PayoutDataEvent> consumer, ApplicationContext applicationContext) {
        IHomeEventBusGroupRegister register = consumer.handleEventsWithPool(PayoutDataInDbHandler.class, 5)
                .thenGroupDoneWithPool(PayoutDataToClearHandler.class, 50)
                .thenGroupDoneWith(applicationContext.getBean(PayoutDataReceiveHandler.class))
                .thenGroupDoneWith(applicationContext.getBean(PayoutDataTallyHandler.class));
    }

}
