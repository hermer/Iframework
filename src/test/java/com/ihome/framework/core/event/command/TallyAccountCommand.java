/**
 * 
 */
package com.ihome.framework.core.event.command;

import org.springframework.stereotype.Component;

import com.ihome.framework.core.event.command.buffer.CommandBuffer.SqlCommandDto;

/**
 * @author zww
 *
 */
public class TallyAccountCommand extends AccountChangeCommand{

    private Long amount;

    private String accountNo;



    public TallyAccountCommand(String requestId, Long amount, String accountNo) {
        super(requestId);
        this.amount = amount;
        this.accountNo = accountNo;
    }



    /* (non-Javadoc)
     * @see com.ihome.framework.core.event.command.buffer.CommandBuffer.SqlCommandDto#toSql()
     */
    @Override
    public String toSql() {
        return new StringBuffer("UPDATE ACCOUNT_INFO SET FROZEN_AMOUNT = FROZEN_AMOUNT-"+amount+",TOTAL_AMOUNT = TOTAL_AMOUNT-"+amount+" WHERE ACCOUNT_NO = "+accountNo+"").toString();
    }
    
   

}
