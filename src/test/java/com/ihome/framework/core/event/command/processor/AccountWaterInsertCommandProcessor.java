/**
 * 
 */
package com.ihome.framework.core.event.command.processor;


import org.springframework.stereotype.Component;

import com.ihome.framework.core.event.IHomeEventBusHandler.EventBusWorkHandler;
import com.ihome.framework.core.event.command.AbstractCommandProcessor;
import com.ihome.framework.core.event.command.AccountChangeCommand;
import com.ihome.framework.core.event.command.AccountWaterInsertCommand;
import com.ihome.framework.core.event.command.CommandEventBusSpringBeanRegister.CommandEvent;
import com.ihome.framework.core.event.command.handler.SimpleCommandEventHandler;
import com.ihome.framework.core.event.command.CommandProcessor;
import com.ihome.framework.core.event.command.SimpleCommandEventBusHandler;

/**
 * @author zww
 *
 */
@Component
public class AccountWaterInsertCommandProcessor extends AbstractCommandProcessor<AccountWaterInsertCommand> implements CommandProcessor<AccountWaterInsertCommand>{


}
