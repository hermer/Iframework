/**
 * 
 */
package com.ihome.framework.core.event.command.buffer;

import org.springframework.stereotype.Component;

import com.ihome.framework.core.event.command.AccountChangeCommand;

/**
 * @author zww
 *
 */
@Component
public class AccountChangeCommandBuffer extends CommandBuffer<AccountChangeCommand> {

    /**
     * @param capacity
     */
    public AccountChangeCommandBuffer() {
        super(256);
    }

}
