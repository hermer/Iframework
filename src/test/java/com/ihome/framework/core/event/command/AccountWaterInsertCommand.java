/**
 * 
 */
package com.ihome.framework.core.event.command;

import com.ihome.framework.core.event.command.buffer.CommandBuffer.SqlCommandDto;

/**
 * @author zww
 *
 */
public class AccountWaterInsertCommand extends SqlCommandDto{

    private Long amount;

    private String accountNo;

    /**
     * @param requestId
     */
    public AccountWaterInsertCommand(String requestId,Long amount,String accountNo) {
        super(requestId);
        this.accountNo = accountNo;
        this.amount = amount;
    }

    /* (non-Javadoc)
     * @see com.ihome.framework.core.event.command.buffer.CommandBuffer.SqlCommandDto#toSql()
     */
    @Override
    public String toSql() {
        return new StringBuffer("INSERT ACCOUNT_WATER VALUES ("+accountNo+", "+amount+",....)").toString();
    }

    /* (non-Javadoc)
     * @see com.ihome.framework.core.event.command.buffer.CommandBuffer.CommandDto#toString()
     */
    @Override
    public String toString() {
        return toSql();
    }

}
