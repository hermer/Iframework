/**
 * 
 */
package com.ihome.framework.core.event.command;

import com.ihome.framework.core.event.command.buffer.CommandBuffer.SqlCommandDto;

/**
 * @author zww
 *
 */
public abstract class AccountChangeCommand extends SqlCommandDto{

    /**
     * @param requestId
     */
    public AccountChangeCommand(String requestId) {
        super(requestId);
    }
}
