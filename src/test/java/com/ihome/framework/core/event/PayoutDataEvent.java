/**
 * 
 */
package com.ihome.framework.core.event;

import com.ihome.framework.core.event.IHomeEventBusHandler.EventDto;

/**
 * @author zww
 *
 */
public class PayoutDataEvent extends EventDto{
    
    private String accountNo; // 账户号
    
    private Integer money;
    
    /**
     * @return the accountNo
     */
    public String getAccountNo() {
        return accountNo;
    }
    /**
     * @param accountNo the accountNo to set
     */
    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }
    
    /**
     * @return the money
     */
    public Integer getMoney() {
        return money;
    }
    
    /**
     * @param money the money to set
     */
    public void setMoney(Integer money) {
        this.money = money;
    }
    
    

}
