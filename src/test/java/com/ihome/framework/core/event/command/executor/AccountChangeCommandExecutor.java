/**
 * 
 */
package com.ihome.framework.core.event.command.executor;

import java.util.concurrent.ThreadLocalRandom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.ihome.framework.core.event.command.AccountChangeCommand;
import com.ihome.framework.core.event.command.CommandExecutor;
import com.ihome.framework.core.event.command.buffer.AccountChangeCommandBuffer;

/**
 * @author zww
 *
 */
@Component
public class AccountChangeCommandExecutor implements CommandExecutor<AccountChangeCommand, AccountChangeCommandBuffer>{
    
    private Logger LOGGER = LoggerFactory.getLogger(getClass());

    /* (non-Javadoc)
     * @see com.ihome.framework.core.event.command.CommandExecutor#execute(com.ihome.framework.core.event.command.buffer.CommandBuffer)
     */
    @Override
    public void execute(AccountChangeCommandBuffer commandBuffer) {
        LOGGER.info("push size[{}]|buffer[{}]",commandBuffer.get().size(),commandBuffer);
        try {
            Thread.sleep(ThreadLocalRandom.current().nextLong(10, 100));
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
