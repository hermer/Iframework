/**
 * 
 */
package com.ihome.framework.core.event.command.lua;

import org.springframework.stereotype.Component;

import com.ihome.framework.core.event.command.lua.LuaCommandUploadProcessor.LuaCommandDto;

/**
 * @author zww
 *
 */
@Component
public class TallyAccountLuaCommandScript extends LuaCommandDto{

    /* (non-Javadoc)
     * @see com.ihome.framework.core.event.command.lua.LuaCommandUploadProcessor.LuaCommandDto#toLuaScript()
     */
    @Override
    public String toLuaScript() {
        return new StringBuffer("local frozenVal = redis.call('hget', KEYS[1],'FrozenMoney');")
                        .append("local totalVal = redis.call('hget', KEYS[1],'TotalMoney');")
                        .append("if(tonumber(frozenVal)>=tonumber(ARGV[1])) then")
                        .append("   redis.call('hset',KEYS[1],'FrozenMoney',frozenVal-tonumber(ARGV[1]));")
                        .append("   redis.call('hset',KEYS[1],'TotalMoney',totalVal-tonumber(ARGV[1]));")
                        .append("   return 1;")
                        .append("else")
                        .append("   return 0;")
                        .append("end").toString();
    }

    

}
