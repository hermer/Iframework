/**
 * 
 */
package com.ihome.framework.core.event.command;

/**
 * @author zww
 *
 */
public class FundOrderUpdateStatusCommand extends FundOrderChangeCommand{

    private String status;
    
    /**
     * @param requestId
     */
    public FundOrderUpdateStatusCommand(String requestId,String status) {
        super(requestId);
        this.status = status;
    }

    /* (non-Javadoc)
     * @see com.ihome.framework.core.event.command.buffer.CommandBuffer.SqlCommandDto#toSql()
     */
    @Override
    public String toSql() {
        return new StringBuffer("UPDATE FUND_ORDER_INFO SET STATE='"+status+"' WHERE ID="+requestId).toString();
    }

}
