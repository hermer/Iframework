///**
// * 
// */
//package com.ihome.framework.core.event;
//
//import com.ihome.framework.core.event.IHomeEventBusHandler.EventBusWorkHandler;
//import com.ihome.framework.core.event.IHomeEventBusHandler.EventDto;
//import com.ihome.framework.core.event.group.IHomeEventBusGroupRegister;
//
//import java.util.concurrent.ThreadLocalRandom;
//
//
///**
// * @author zww
// *      --> c11 --> c12
// * A                     --> P
// *      --> c21 --> c22
// *
// */
//public class ChainEventMainTest {
//    
//    
//    public static class ChainEvent extends EventDto{
//        private long data;
//        
//        /**
//         * @return the data
//         */
//        public long getData() {
//            return data;
//        }
//        /**
//         * @param data the data to set
//         */
//        public void setData(long data) {
//            this.data = data;
//        }
//    }
//    
//    public static class PWorkHandler extends EventBusWorkHandler<ChainEvent>{
//
//        /* (non-Javadoc)
//         * @see com.ihome.framework.core.event.IHomeEventBusHandler.EventBusHandler#onEvent(com.ihome.framework.core.event.IHomeEventBusHandler.EventDto)
//         */
//        @Override
//        public void handler(ChainEvent e) throws Exception{
//            Thread.sleep(ThreadLocalRandom.current().nextLong(0, 1000));
//            System.out.println("Thread id -> "+Thread.currentThread().getId()+" p -> "+e.getData()+" - "+System.currentTimeMillis());
//        }
//    }
//    
//    public static class C11WorkHandler extends EventBusWorkHandler<ChainEvent>{
//
//        /* (non-Javadoc)
//         * @see com.ihome.framework.core.event.IHomeEventBusHandler.EventBusHandler#onEvent(com.ihome.framework.core.event.IHomeEventBusHandler.EventDto)
//         */
//        @Override
//        public void handler(ChainEvent e) throws Exception{
//            Thread.sleep(ThreadLocalRandom.current().nextLong(0, 1000));
//            System.out.println("Thread id -> "+Thread.currentThread().getId()+" c11 -> "+e.getData()+" - "+System.currentTimeMillis());
//        }
//    }
//    
//    public static class C21WorkHandler extends EventBusWorkHandler<ChainEvent>{
//
//        /* (non-Javadoc)
//         * @see com.ihome.framework.core.event.IHomeEventBusHandler.EventBusHandler#onEvent(com.ihome.framework.core.event.IHomeEventBusHandler.EventDto)
//         */
//        @Override
//        public void handler(ChainEvent e) throws Exception{
//            Thread.sleep(ThreadLocalRandom.current().nextLong(0, 1000));
//            System.out.println("Thread id -> "+Thread.currentThread().getId()+" c21 -> "+e.getData()+" - "+System.currentTimeMillis());
//        }
//    }
//    
//    
//    public static class AHandler extends EventBusWorkHandler<ChainEvent>{
//
//        /* (non-Javadoc)
//         * @see com.ihome.framework.core.event.IHomeEventBusHandler.EventBusHandler#onEvent(com.ihome.framework.core.event.IHomeEventBusHandler.EventDto)
//         */
//        @Override
//        public void handler(ChainEvent e) throws Exception {
//            Thread.sleep(ThreadLocalRandom.current().nextLong(0, 1000));
//            System.out.println("a -> "+e.getData()+" - "+System.currentTimeMillis());
//        }
//    }
//    
//    public static class PHandler extends EventBusWorkHandler<ChainEvent>{
//
//        /* (non-Javadoc)
//         * @see com.ihome.framework.core.event.IHomeEventBusHandler.EventBusHandler#onEvent(com.ihome.framework.core.event.IHomeEventBusHandler.EventDto)
//         */
//        @Override
//        public void handler(ChainEvent e) throws Exception {
//            Thread.sleep(ThreadLocalRandom.current().nextLong(0, 1000));
//            System.out.println("p -> "+e.getData()+" - "+System.currentTimeMillis());
//        }
//    }
//    
//    public static class C11Handler extends EventBusWorkHandler<ChainEvent>{
//
//        /* (non-Javadoc)
//         * @see com.ihome.framework.core.event.IHomeEventBusHandler.EventBusHandler#onEvent(com.ihome.framework.core.event.IHomeEventBusHandler.EventDto)
//         */
//        @Override
//        public void handler(ChainEvent e) throws Exception {
//            Thread.sleep(ThreadLocalRandom.current().nextLong(0, 1000));
//            System.out.println("c11 -> "+e.getData()+" - "+System.currentTimeMillis());
//        }
//    }
//    
//    public static class C12Handler extends EventBusWorkHandler<ChainEvent>{
//
//        /* (non-Javadoc)
//         * @see com.ihome.framework.core.event.IHomeEventBusHandler.EventBusHandler#onEvent(com.ihome.framework.core.event.IHomeEventBusHandler.EventDto)
//         */
//        @Override
//        public void handler(ChainEvent e) throws Exception {
//            Thread.sleep(ThreadLocalRandom.current().nextLong(0, 2000));
//            System.out.println("c12 -> "+e.getData()+" - "+System.currentTimeMillis());
//        }
//    }
//    
//    public static class C21Handler extends EventBusWorkHandler<ChainEvent>{
//
//        /* (non-Javadoc)
//         * @see com.ihome.framework.core.event.IHomeEventBusHandler.EventBusHandler#onEvent(com.ihome.framework.core.event.IHomeEventBusHandler.EventDto)
//         */
//        @Override
//        public void handler(ChainEvent e) throws Exception {
//            Thread.sleep(ThreadLocalRandom.current().nextLong(0, 1000));
//            System.out.println("c21 -> "+e.getData()+" - "+System.currentTimeMillis());
//        }
//    }
//    
//    public static class C22Handler extends EventBusWorkHandler<ChainEvent>{
//
//        /* (non-Javadoc)
//         * @see com.ihome.framework.core.event.IHomeEventBusHandler.EventBusHandler#onEvent(com.ihome.framework.core.event.IHomeEventBusHandler.EventDto)
//         */
//        @Override
//        public void handler(ChainEvent e) throws Exception {
//            Thread.sleep(ThreadLocalRandom.current().nextLong(0, 2000));
//            System.out.println("c22 -> "+e.getData()+" - "+System.currentTimeMillis());
//        }
//    }
//    
//    
//    
//    public static class C31Handler extends EventBusWorkHandler<ChainEvent>{
//
//        /* (non-Javadoc)
//         * @see com.ihome.framework.core.event.IHomeEventBusHandler.EventBusHandler#onEvent(com.ihome.framework.core.event.IHomeEventBusHandler.EventDto)
//         */
//        @Override
//        public void handler(ChainEvent e) throws Exception {
//            Thread.sleep(ThreadLocalRandom.current().nextLong(0, 1000));
//            System.out.println("c31 -> "+e.getData()+" - "+System.currentTimeMillis());
//        }
//    }
//    
//    
//
//    public static class C41Handler extends EventBusWorkHandler<ChainEvent>{
//
//        /* (non-Javadoc)
//         * @see com.ihome.framework.core.event.IHomeEventBusHandler.EventBusHandler#onEvent(com.ihome.framework.core.event.IHomeEventBusHandler.EventDto)
//         */
//        @Override
//        public void handler(ChainEvent e) throws Exception {
//            Thread.sleep(ThreadLocalRandom.current().nextLong(0, 1000));
//            System.out.println("c41 -> "+e.getData()+" - "+System.currentTimeMillis());
//        }
//    }
//    
//    
//    
//    public static class ChainEventBusHandler extends IHomeEventBusHandler<ChainEvent>{
//        public ChainEventBusHandler() throws Exception{
//            afterPropertiesSet();
//        }
//
//        /* (non-Javadoc)
//         * @see com.ihome.framework.core.event.IHomeEventConsumer#handleEventsWith(com.ihome.framework.core.event.IHomeEventConsumer)
//         */
//        @Override
//        public void handleEventsWith(IHomeEventConsumer<ChainEvent> consumer) {
//            // TODO Auto-generated method stub
//            
//        }
//    }
//    
//    
//    public static void main(String[] args) throws Exception {
//        
//        
//        System.out.println("test chain events begin");
//        System.out.println("     --> c11 --> c12");
//        System.out.println(" A                     --> P");
//        System.out.println("     --> c21 --> c22");
//        
//        ChainEventBusHandler busHandler = new ChainEventBusHandler();
//        IHomeEventConsumer<ChainEvent> consumer = busHandler; 
//        PHandler pHandler = new PHandler();
//        
//        C12Handler c12 = new C12Handler();
//        C22Handler c22 = new C22Handler();
//        
//        C11Handler c11 = new C11Handler();
//        C21Handler c21 = new C21Handler();
//        
//        consumer.handleEventsWith(new AHandler()).thenGroupDoneWith(c11,c21);
//        consumer.afterHandleEventsWith(new EventBusWorkHandler[]{c11}, c12)
//            .afterHandleEventsWith(new EventBusWorkHandler[]{c21}, c22)
//            .afterHandleEventsWith(new EventBusWorkHandler[]{c12,c22}, pHandler).build();
//        
//        
//        
////        consumer.handleEventsWith(new C11Handler()).thenHandlerGroupDone(c12);
////        consumer.handleEventsWith(new C21Handler()).thenHandlerGroupDone(c22);
////        consumer.afterHandleEventsWith(new EventBusHandler[]{c12,c22}, pHandler).build();
//        
//        IHomeEventProducer<ChainEvent> producer =  busHandler;
//        producer.publishEvent(new IHomeEventTranslator<ChainEventMainTest.ChainEvent>() {
//            
//            @Override
//            public void translateTo(ChainEvent e) {
//                e.setData(ThreadLocalRandom.current().nextLong());
//            }
//        });
//        Thread.sleep(2000L);
//        busHandler.destroy();
//        
//        System.out.println("test chain events end");
//        System.out.println("=====================================");
//        
//        
//        
//        
//        System.out.println("test line events begin");
//        System.out.println("P --> c11 --> c12 -> c21 -> c22");
//        
//        busHandler = new ChainEventBusHandler();
//        consumer = busHandler; 
//        pHandler = new PHandler();
//        
//        c12 = new C12Handler();
//        c22 = new C22Handler();
//        c11 = new C11Handler();
//        c21 = new C21Handler();
//        
//        
//        
//        consumer.handleEventsWith(pHandler).thenGroupDoneWith(c11)
//            .thenGroupDoneWith(c12).thenGroupDoneWith(c21)
//            .thenGroupDoneWith(c22);
//        consumer.build();
//        
//        producer =  busHandler;
//        producer.publishEvent(new IHomeEventTranslator<ChainEventMainTest.ChainEvent>() {
//            
//            @Override
//            public void translateTo(ChainEvent e) {
//                e.setData(ThreadLocalRandom.current().nextLong());
//            }
//        });
//        Thread.sleep(2000L);
//        busHandler.destroy();
//        
//        System.out.println("test line events end");
//        System.out.println("=====================================");
//        
//        
//        
//        System.out.println("test parallel events begin");
//        System.out.println("         --> C11 ");
//        System.out.println("         --> C12 ");
//        System.out.println("P ");
//        System.out.println("         --> C21 ");
//        System.out.println("         --> C22 ");
//        
//        busHandler = new ChainEventBusHandler();
//        consumer = busHandler; 
//        pHandler = new PHandler();
//        
//        c12 = new C12Handler();
//        c22 = new C22Handler();
//        c11 = new C11Handler();
//        c21 = new C21Handler();
//        
//        
//        
//        consumer.handleEventsWith(pHandler)
//            .thenGroupDoneWith(c11,c12,c22,c21);
//        
//        consumer.build();
//        
//        producer =  busHandler;
//        producer.publishEvent(new IHomeEventTranslator<ChainEventMainTest.ChainEvent>() {
//            
//            @Override
//            public void translateTo(ChainEvent e) {
//                e.setData(ThreadLocalRandom.current().nextLong());
//            }
//        });
//        Thread.sleep(2000L);
//        busHandler.destroy();
//        
//        System.out.println("test parallel events end");
//        System.out.println("=====================================");
//        
//        
//        
//        // 并发消费 
//        System.out.println("test multi Consumer begin");
//        System.out.println("         --> C11(multi) --> c41(single)");
//        System.out.println("P(multi)                                 --> A(multi)");
//        System.out.println("         --> C21(multi) --> c31(single)");
//        
//        busHandler = new ChainEventBusHandler();
//        consumer = busHandler; 
//        
//        IHomeEventBusGroupRegister<ChainEvent> register = consumer.handleEventsWithPool(new PWorkHandler(),new PWorkHandler(),
//                new PWorkHandler(),new PWorkHandler());
//        
//        C31Handler c31 = new C31Handler();
//        C41Handler c41 = new C41Handler();
//        
//        register.thenGroupDoneWithPool(new C21WorkHandler(),new C21WorkHandler()).thenGroupDoneWith(c31);
//        register.thenGroupDoneWithPool(new C11WorkHandler(),new C11WorkHandler()).thenGroupDoneWith(c41);
//        
//        
//        
//        consumer.afterHandleEventsWithPool(new EventBusWorkHandler[]{c31,c41}, new AHandler(),new AHandler());
//        
//        consumer.build();
//        
//        producer =  busHandler;
//        
//        for (int i = 0; i < 4; i++) {
//            producer.publishEvent(new IHomeEventTranslator<ChainEventMainTest.ChainEvent>() {
//                
//                @Override
//                public void translateTo(ChainEvent e) {
//                    e.setData(ThreadLocalRandom.current().nextLong());
//                }
//            });
//        }
//        
//        Thread.sleep(2000L);
//        busHandler.destroy();
//        
//        System.out.println("test multi events end");
//        System.out.println("=====================================");
//        
//        
//        
//        
//        
//        
//        
//        
//        
//    }
//    
//    
//    
//
//}
