/**
 * 
 */
package com.ihome.framework.core.event;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

import com.ihome.framework.core.event.IHomeEventBusHandler.EventBusWorkHandler;
import com.ihome.framework.core.event.IHomeEventBusHandler.EventDto;
import com.ihome.framework.core.event.command.AccountChangeCommand;
import com.ihome.framework.core.event.command.AccountWaterInsertCommand;
import com.ihome.framework.core.event.command.CommandEventBusSpringBeanRegister.CommandEvent;
import com.ihome.framework.core.event.command.FrozenAccountCommand;
import com.ihome.framework.core.event.command.FundOrderInsertCommand;
import com.ihome.framework.core.event.command.SimpleCommandEventBusHandler;
import com.ihome.framework.core.event.command.processor.AccountChangeCommandProcessor;
import com.ihome.framework.core.event.command.processor.AccountWaterInsertCommandProcessor;
import com.ihome.framework.core.utils.JsonUtil;
import com.lmax.disruptor.dsl.ProducerType;


/**
 * @author zww
 *
 */
public class AccountPayoutEventMainTest {
//
//
//    private static HashMap<String, Account> CASHS = new HashMap<String, Account>();    
//
//    private static Logger LOGGER = LoggerFactory.getLogger(AccountPayoutEventMainTest.class);
//
//    private static AtomicLong LOG = new AtomicLong(0);
//
//    public static class PayoutDataInDbHandler extends EventBusWorkHandler<PayoutDataEvent>{
//        /* (non-Javadoc)
//         * @see com.ihome.framework.core.event.IHomeEventBus#onEvent(com.ihome.framework.core.event.IHomeEventBus.EventDto)
//         */
//        @Override
//        public void handler(PayoutDataEvent e) throws Exception {
//
//            long threadId = Thread.currentThread().getId(); // 获取当前线程id
//            String accountNo = e.getAccountNo();
//            Integer money = e.getMoney();
//            Account account = CASHS.get(accountNo);
//            if(account.getTotalMoney()-account.getFrozenMoney()<money){
//                throw new RuntimeException("可用余额不足..");
//            }else{
//                account.setFrozenMoney(account.getFrozenMoney()+money);
//                e.addCommand(new FundOrderInsertCommand(e.getRequestId(),(long)e.getMoney(), accountNo));
//                e.addCommand(new FrozenAccountCommand(e.getRequestId(), (long)e.getMoney(), accountNo));
//                e.addCommand(new AccountWaterInsertCommand(e.getRequestId(), (long)e.getMoney(), accountNo));
//                LOGGER.info(String.format("UUID %s | Thread Id %s 数据落表并冻结金额[account:%s|status:PRE_SEND] ....",e.getRequestId(),threadId, account));
//            }
//        }
//    }
//
//    public static class PayoutDataToClearHandler extends EventBusWorkHandler<PayoutDataEvent>{
//        /* (non-Javadoc)
//         * @see com.ihome.framework.core.event.IHomeEventBusHandler#onEvent(com.ihome.framework.core.event.IHomeEventBusHandler.EventDto)
//         */
//        @Override
//        public void handler(PayoutDataEvent e) throws Exception {
//            long threadId = Thread.currentThread().getId(); // 获取当前线程id
//            String accountNo = e.getAccountNo();
//            Integer money = e.getMoney();
//            Account account = CASHS.get(accountNo);
//            LOGGER.info(String.format("UUID %s | Thread Id %s 调用清结算[account:%s|status:DOING] ....",e.getRequestId(),threadId, account));
//        }
//    }
//
//
//    public static class PayoutDataReceiveHandler extends EventBusWorkHandler<PayoutDataEvent>{
//        /* (non-Javadoc)
//         * @see com.ihome.framework.core.event.IHomeEventBusHandler#onEvent(com.ihome.framework.core.event.IHomeEventBusHandler.EventDto)
//         */
//        @Override
//        public void handler(PayoutDataEvent e) throws Exception {
//            long threadId = Thread.currentThread().getId(); // 获取当前线程id
//            String accountNo = e.getAccountNo();
//            Integer money = e.getMoney();
//            Account account = CASHS.get(accountNo);
//            LOGGER.info(String.format("UUID %s | Thread Id %s 清结算响应成功[account:%s|status:SUCCESS] ........",e.getRequestId(),threadId, account));
//        }
//    }
//
//
//    public static class PayoutDataTallyHandler extends EventBusWorkHandler<PayoutDataEvent>{
//        /* (non-Javadoc)
//         * @see com.ihome.framework.core.event.IHomeEventBusHandler#onEvent(com.ihome.framework.core.event.IHomeEventBusHandler.EventDto)
//         */
//        @Override
//        public void handler(PayoutDataEvent e) throws Exception {
//            long threadId = Thread.currentThread().getId(); // 获取当前线程id
//            String accountNo = e.getAccountNo();
//            Integer money = e.getMoney();
//            Account account = CASHS.get(accountNo);
//
//            Long temp = account.getTotalMoney();
//            Thread.sleep(1L);
//            if(account.getFrozenMoney() >= money && CASHS.get(accountNo).getTotalMoney() == temp){
//                account.setFrozenMoney(account.getFrozenMoney()-money);
//                account.setTotalMoney(account.getTotalMoney()-money);
//                LOG.incrementAndGet();
//                LOGGER.info(String.format("UUID %s | Thread Id %s 解冻金额并记账[account:%s|status:TALLY] ........",e.getRequestId(),threadId, account));
//            }else{
//                LOGGER.info(String.format("UUID %s | Thread Id %s 账户余额不足记账错误[account:%s|status:ERROR] ........",e.getRequestId(),threadId,account));
//            }
//
//        }
//    }
//
//    public static class ParkingDataEventBusHandler extends IHomeEventBusHandler<PayoutDataEvent>{
//        public ParkingDataEventBusHandler() throws Exception{
//            super(1024, ProducerType.MULTI);
//            afterPropertiesSet();
//        }
//        /* (non-Javadoc)
//         * @see com.ihome.framework.core.event.IHomeEventBusHandler#handleEventException(java.lang.Throwable, long, com.ihome.framework.core.event.IHomeEventBusHandler.EventDto)
//         */
//        @Override
//        public void handleEventException(Throwable ex, long sequence, PayoutDataEvent event) {
//            // LOGGER.error(String.format("UUID %s sequence %s | Thread Id %s error %s ........",event.getRequestId(),sequence,Thread.currentThread().getId(),ex.getMessage()));
//            event.setSuccess(false);
//            event.setErrorMessage(ex.getMessage());
//        }
//        /* (non-Javadoc)
//         * @see com.ihome.framework.core.event.IHomeEventConsumer#handleEventsWith(com.ihome.framework.core.event.IHomeEventConsumer)
//         */
//        @Override
//        public void handleEventsWith(IHomeEventConsumer<PayoutDataEvent> consumer) {
//            
//        }
//    }
//
//
//    public static void main(String[] args) throws Exception {
//
//        
////
////        final Long accountMoney = 10000L;
////
////        int threadSize = 20;
////        // 1. 生成账户信息
////        for (int i = 0; i < 2; i++) {
////
////            Account account = new Account();
////            account.setAccountNo(ThreadLocalRandom.current().nextLong(1, 100000));
////            account.setFrozenMoney(0L);
////            account.setTotalMoney(accountMoney);
////            CASHS.put(account.getAccountNo()+"", account);
////        }
////
////        System.out.println("生成账户信息|"+CASHS.values().toString());
////
////
////        ParkingDataEventBusHandler busHandler = new ParkingDataEventBusHandler();
////
////
////        IHomeEventConsumer<PayoutDataEvent> consumer = busHandler;
////
////        final PayoutDataInDbHandler dbHandler = new PayoutDataInDbHandler();
////        final PayoutDataToClearHandler clearHandler = new PayoutDataToClearHandler();
////        final PayoutDataReceiveHandler receiveHandler = new PayoutDataReceiveHandler();
////        final PayoutDataTallyHandler tallyHandler = new PayoutDataTallyHandler();
////
////        consumer.handleEventsWith(dbHandler)
////        .thenGroupDoneWith(clearHandler)
////        .thenGroupDoneWith(receiveHandler)
////        .thenGroupDoneWith(tallyHandler);
////        consumer.build();
////
////        final IHomeEventProducer<PayoutDataEvent> producer = busHandler;
////
////        final String[] accountNos = CASHS.keySet().toArray(new String[]{});
////
////        ExecutorService executorService = Executors.newFixedThreadPool(threadSize);
////
////        final CountDownLatch countDownLatch = new CountDownLatch(threadSize*100);
////
////        for (int i = 0; i < threadSize; i++) {
////            executorService.submit(new Runnable() {
////                @Override
////                public void run() {
////                    for (int i = 0; i<100; i++) {
////                        producer.publishEvent(new IHomeEventTranslator<PayoutDataEvent>() {
////
////                            @Override
////                            public void translateTo(PayoutDataEvent e) {
////                                e.setAccountNo(accountNos[ThreadLocalRandom.current().nextInt(0, accountNos.length)]);
////                                e.setMoney(100);
////                            }
////                        });
////                        countDownLatch.countDown();
////                    } 
////                }
////            });
////        }
////        countDownLatch.await();
////        busHandler.destroy();
////        executorService.shutdownNow();
////        System.out.println( LOG.get());
//
//        //
//        //        LOG.set(0);
//
//
//        //        for (int i = 0; i < threadSize; i++) {
//        //            executorService.submit(new Runnable() {
//        //                @Override
//        //                public void run() {
//        //                    for (int i = 0; i<100; i++) {
//        //                        try{
//        //                            PayoutDataEvent e = new PayoutDataEvent();
//        //                            e.setAccountNo(accountNos[ThreadLocalRandom.current().nextInt(0, accountNos.length)]);
//        //                            e.setMoney(100);
//        //                            dbHandler.onEvent(e);
//        //                            clearHandler.onEvent(e);
//        //                            receiveHandler.onEvent(e);
//        //                            tallyHandler.onEvent(e);
//        //                        }catch (Exception e) {
//        //
//        //                        }
//        //                        countDownLatch.countDown();
//        //                    } 
//        //                }
//        //            });
//        //        }
//        //
//        //        countDownLatch.await();
//        //        busHandler.destroy();
//        //        executorService.shutdownNow();
//        //        System.out.println( LOG.get());
//
//
//
//
//
//
//
//
//
//
//
//    }
//
//
    
    public static Class<?> getActualTypeArgument(Class<?> clazz) {
        Class<?> entitiClass = null;
        Type genericSuperclass = clazz.getGenericSuperclass();
        if (genericSuperclass instanceof ParameterizedType) {
            Type[] actualTypeArguments = ((ParameterizedType) genericSuperclass)
                    .getActualTypeArguments();
            if (actualTypeArguments != null && actualTypeArguments.length > 0) {
                entitiClass = (Class<?>) actualTypeArguments[0];
            }
        }

        return entitiClass;
    }


    
    public static void main(String[] args) throws Exception {
//        SimpleCommandEventBusHandler<AccountChangeCommand> processor = new SimpleCommandEventBusHandler<AccountChangeCommand>();
        System.out.println(JsonUtil.parseArray("['3000208559']", String.class).get(0));
        
//        processor.afterPropertiesSet();
//        System.out.println(processor);
        
    }
}
