/**
 * 
 */
package com.ihome.framework.core.event.command.buffer;

import org.springframework.stereotype.Component;

import com.ihome.framework.core.event.command.FundOrderChangeCommand;
import com.ihome.framework.core.event.command.FundOrderInsertCommand;

/**
 * @author zww
 *
 */
@Component
public class FundOrderChangeCommandBuffer extends CommandBuffer<FundOrderChangeCommand>{

    /**
     * @param capacity
     */
    public FundOrderChangeCommandBuffer() {
        super(256);
    }

}
