/**
 * 
 */
package com.ihome.framework.core.event.command.buffer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.ihome.framework.core.event.command.AccountWaterInsertCommand;
import com.ihome.framework.core.event.command.buffer.CommandBuffer.CommandDto;
import com.ihome.framework.core.exception.PlatformException;
import com.ihome.framework.core.id.service.IdGeneratorService.UUIDWorker;
import com.ihome.framework.core.log.Log;

/**
 * @author zww
 *
 */
@Component
public  class AccountWaterInsertCommandBuffer extends CommandBuffer<AccountWaterInsertCommand>{

    /**
     * @param capacity
     */
    public AccountWaterInsertCommandBuffer() {
        super(256);
    }

}
