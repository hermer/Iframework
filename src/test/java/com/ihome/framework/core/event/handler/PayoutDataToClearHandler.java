/**
 * 
 */
package com.ihome.framework.core.event.handler;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

//import com.ihome.framework.core.event.Account;
import com.ihome.framework.core.event.PayoutDataEvent;
import com.ihome.framework.core.event.SpringTestCase;
import com.ihome.framework.core.event.IHomeEventBusHandler.EventBusWorkHandler;
import com.ihome.framework.core.event.command.FundOrderUpdateStatusCommand;

/**
 * @author zww
 *
 */
@Component
@Scope("prototype")
public  class PayoutDataToClearHandler extends EventBusWorkHandler<PayoutDataEvent>{
    /* (non-Javadoc)
     * @see com.ihome.framework.core.event.IHomeEventBusHandler#onEvent(com.ihome.framework.core.event.IHomeEventBusHandler.EventDto)
     */
    
    private Random random = new Random();
    
    @Override
    public void handler(PayoutDataEvent e,long sequence,boolean endOfBatch) throws Exception {
        long threadId = Thread.currentThread().getId(); // 获取当前线程id
        
        Thread.sleep(random.nextInt(200));
        e.addCommand(new FundOrderUpdateStatusCommand(e.getRequestId(),"DOING"));
        LOGGER.info(String.format("UUID %s | Thread Id %s 调用清结算[account:%s|status:DOING] ....",e.getRequestId(),threadId, 
                ""));
    }
}
