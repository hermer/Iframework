package com.ihome.framework.core.nos;

import static org.junit.Assert.assertTrue;

import org.apache.commons.lang3.StringUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import com.netease.cloud.auth.BasicCredentials;
import com.netease.cloud.auth.Credentials;
import com.netease.cloud.services.nos.NosClient;

public class NosAuthToolTest {

    static NosClient client = null;
    static String accessKey = "2d98d96564f64b038eb69d81fa84f7xx";
    static String secretKey = "1f29c502cc5d4227b40f3ffcd885ebxx";

    @BeforeClass
    public static void beforeClass() {
        Credentials credentials = new BasicCredentials(accessKey, secretKey);
        client = new NosClient(credentials);
    }

    @Test
    public void testGetUploadToken() {
        NosUpdatePolicy policy = new NosUpdatePolicy("auto-deploy-test","testKey", 60);
        policy = policy.withCallBackUrl("http://www.baidu.com");
        policy = policy.withCallBackBody("auto-deploy-test&testKey&abc=1234");
        String token = NosAuthTool.getUploadToken(client, policy.toJson(), accessKey);
        assertTrue(StringUtils.isNotBlank(token));
    }

    @Test
    public void testValidateStringSignature() {
        String reqUrl = "http://www.baidu.com";
        String reqBody = "auto-deploy-test&testKey&abc=1234";
        String encodedSign = "WWSLubMTpx+n0CUbu7z2TjXbppiO7DpANdoShTADOKM=";
        assertTrue(NosAuthTool.validateStringSignature(client, reqUrl, reqBody, encodedSign));
    }
}
