package com.ihome.framework.core.nos;

import static org.junit.Assert.fail;

import java.net.URL;
import java.util.Date;

import org.junit.BeforeClass;
import org.junit.Test;

import com.netease.cloud.auth.BasicCredentials;

public class IHomeNosClientTest {

//    public static final String nosHostName = "nos.126.net";
//    public static final String accessKey = "a482b6932c684630b4c0e6390b69da6c";
//    public static final String secretKey = "8d71ee2b54c24eb0aebddcf9ee4e3e33";
//    public static final String publicBucket = "zsy-dev-public";
    
    public static final String nosHostName = "nos.126.net";
    public static final String accessKey = "236f8df3a64149b494275e5ddf2aa87c";
    public static final String secretKey = "cdeb03cb2dc44038878eb8cda396086c";
    public static final String publicBucket = "zsy-dev-public";
    public static final String privateBucket = "zsy-test-private";
    
    private static IHomeNosClient client = null;
    
    @BeforeClass
    public static void beforeClass() {
        BasicCredentials credentials = new BasicCredentials(accessKey, secretKey);
        client = new IHomeNosClient(credentials, nosHostName);
    }
    
    @Test
    public void testGetObjectStringStringFile() {
        fail("Not yet implemented");
    }

    @Test
    public void testGetObjectAsString() {
        String content = client.getObjectAsString(publicBucket, "test.ftl");
        System.out.println(content);
    }
    
    @Test
    public void genUrl() {
       String key = "e509907e-8963-4bec-a6e7-aa9ce01ac0a6";
       URL url = client.generatePresignedUrl(privateBucket, key, new Date(System.currentTimeMillis() + 30000));
       String baseUrl = "http://" + nosHostName + "/" + privateBucket + "/" + key;
       String reqUrl = url.getQuery();
       System.out.println(baseUrl + "?" +  reqUrl);
    }

}
